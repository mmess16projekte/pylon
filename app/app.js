
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var package = require('./package');

var contextMiddleware = require('./middleware/contextSetter')({
  environment:app.get('env'),
  version:package.version
});

var main = require('./src/main.js')({
  socketio:io
}).init();

app.use(express.static('public'));
app.use(contextMiddleware);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

app.get('/', function(req, res){
  res.render('index');
});
app.get('/start',function(req,res){
  main.start();
  res.redirect('/');
});
app.get('/stop',function(req,res){
  main.stop();
  res.end('game simulation stopped');
});



http.listen(3000, function(){
  console.log('listening on localhost:3000');

});

