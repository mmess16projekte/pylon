/* global PylonClient, EventPublisher, io */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.WebSocket
 * @description Module that interacts with socket.io. Receives data from the 
 *    server and passes it on to the Deserializer. 
 */
PylonClient.WebSocket = function(){
  var that = new EventPublisher();
  var socket;
  var initdata;

  function sendInput(action,state){
    socket.emit('input',{
      a:action,
      s:state
    });
  }

  function login(data){
    socket.emit('login',data);
  }

  function init(){
    socket = io();
    socket.on('connect',function(){
      console.log('socket connect');
    });
    socket.on('init',function(data){
      initdata = data;
      that.notifyAll('ready');
    });
    socket.on('death',function(data){
      that.notifyAll('death',data);
    });
    socket.on('disconnect',function(){
      that.notifyAll('disconnect');
      console.log('socket disconnect');
    });
    socket.on('error',function(error){
      console.log(error);
    });
    socket.on('update',function(data){
      that.notifyAll('update',data);
    });
    socket.on('debug',function(message){
      console.log(message);
    });

    return that;
  }
  function getId(){
    if(!initdata){
      return null;
    }
    return initdata.id;
  }


  that.init = init;
  that.sendInput = sendInput;
  that.login = login;
  that.getId = getId;
  
  return that;
};