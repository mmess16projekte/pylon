/* global PylonClient, EventPublisher */
/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.InputHandler
 * @description Maps keyboard inputs to actions that get sent to the server. 
 */
PylonClient.InputHandler = function(options){
  var that = new EventPublisher();
  var keyboardListener;

  var keyCodes = {
    38:'up',
    87:'up',
    37:'left',
    65:'left',
    39:'right',
    68:'right',
    40:'down',
    83:'down',
    69:'fire',
    32:'fire'
  };
  var currentState={
  };

  function initListeners(){
    Object.keys(keyCodes).forEach(function(code){
      keyboardListener.addKeyCodeListener(code,function(state){
        var action = keyCodes[code];
        if(currentState[action] == state){
          return;
        }
        currentState[action] = state;
        that.notifyAll('input',{
          action:action,
          state:state
        });
      });
    });
  }
 
  function init(){
    keyboardListener = options.keyboardListener;
    initListeners();
    return that;
  }
  that.init = init;
  return that;
};