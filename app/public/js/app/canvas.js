/* global PylonClient, createjs */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.Canvas
 * @description Responsible for drawing, interfaces with EaselJS. 
 *    Uses a moving container to draw all world objects and to simulate a moving
 *    camera. Passes "meta" events to the UI canvas component. 
 */
PylonClient.Canvas = function(options){
  var that = {};
  var canvas;
  var scalingFactor;
  var gridSize = 80;
  var canvasUI;

  
  var stage;
  var world;
  var actorShapes = {};
  var grid;

  //var camera = [0,0];
  var cameraTarget;


  function resizeToWindow(){
    var width = window.innerWidth;
    var height = window.innerHeight;
   

    var desiredAspectRatio = 16/9;
    var windowAspectRatio = width/height;

    if(windowAspectRatio > desiredAspectRatio){
      width = height*desiredAspectRatio;
    } else {
      height = width/desiredAspectRatio;
    }
    canvas.width = width;
    canvas.height = height;

    scalingFactor = (width/1440)*8;
    world.scaleX=scalingFactor;
    world.scaleY=scalingFactor;
    
    canvas.style.marginTop = (-height/2)+'px';
    canvas.style.marginLeft = (-width/2)+'px';
    

    canvasUI.resize(width,height);
    reset();
  }

  function reset(){
    stage.removeAllChildren();
    world.removeAllChildren();
    stage.clear();
    stage.addChild(world);
    stage.addChild(canvasUI.container);
    Object.keys(actorShapes).forEach(function(id){
      addShapeToWorld(actorShapes[id]);
    });
    grid = createGrid();
    stage.addChild(grid);
    stage.update();
  }
  function clear(){
    actorShapes = {};
    reset();
  }

  function update(eventList){
    var events = eventList || [];
    var shape;
    var actor;
    events.forEach(function(event){
      actor = event.actor;
      switch(event.type){
      case 'create':
        shape = createShape(actor);
        actorShapes[actor.id] = shape;
        addShapeToWorld(shape);
        break;
      case 'update':
        shape = actorShapes[actor.id];
        updateShape(shape,actor);
        break;
      case 'destroy':
        shape = actorShapes[actor.id];
        removeShape(shape,actor);
        break;
      case 'meta':
        canvasUI.update(event.meta);
        break;
      default:
        break;
      }
    });
    stage.update();
  }
  function addShapeToWorld(shape){
    world.addChild(shape);
    if(shape.label){
      world.addChild(shape.label);
    }
  }

  function createShape(actor){
    var com = actor.com;
    var path = actor.staticData.path;
    var text = actor.staticData.text;
    var shape;
    if(text){
      shape = createText(text,com,actor.staticData);
    } else{
      shape = createPolygon(path,com,actor.staticData);
    }
    if(actor.name){
      shape.label = createLabel(actor.name);
    }
    if(actor.cid === cameraTarget){
      centerCamera(actor.p);
    }
    return shape;
  }
  function updateShape(shape,actor){
    var pos = actor.p;
    var angle = actor.a;
    var degrees = angle * 180 / Math.PI;
    shape.x = pos[0];
    shape.y = pos[1];
    shape.rotation = degrees;
    if(shape.label){
      shape.label.x = shape.x;
      shape.label.y = shape.y+6;
    }
    if(actor.cid === cameraTarget){
      moveCamera(actor.p);
    }
  }
  function removeShape(shape,actor){
    world.removeChild(shape);
    if(shape.label){
      world.removeChild(shape.label);
    }
    delete actorShapes[actor.id];

  }

  function createPolygon(path,center,options){
    var polygon = new createjs.Shape();
    var i;
    var fillColor = options.fillColor || 'red';
    var strokeColor = options.strokeColor || 'black';
    polygon.graphics.setStrokeStyle(0.1).beginFill(fillColor).beginStroke(strokeColor);
    polygon.graphics.moveTo(path[0][0],path[0][1]);
    for(i=1;i<path.length;i++){
      polygon.graphics.lineTo(path[i][0],path[i][1]);
    }
    polygon.graphics.closePath();
    polygon.regX = center[0];
    polygon.regY = center[1];
    return polygon;
  }
  function createLabel(name){
    var label = new createjs.Text();
    label.set({
      font: '2px Arial',
      text: name,
      textAlign: 'center'
    });
    return label;
  }

  function createText(text,center,options){
    var fillColor = options.fillColor || 'red';
    var size = options.textSize || '3px';
    var font = 'Arial';
    var textShape = new createjs.Text();
    textShape.set({
      font: size+' '+font,
      text: text,
      textAlign: 'center',
      color:fillColor
    });
    textShape.regX = center[0];
    textShape.regY = center[1];
    return textShape;
  }

  function createGrid(){
    var grid = new createjs.Shape();
    var startx = -100;
    var endx = canvas.width+100;
    var starty = -100;
    var endy = canvas.height+100;
    var delta = gridSize;
    var x;
    var y;

    grid.graphics.setStrokeStyle(0.2);
    grid.graphics.beginStroke('#CCC');
    grid.graphics.moveTo(startx,starty);
    for(x = startx;x<=endx;x+=delta){
      grid.graphics.lineTo(x,starty);
      grid.graphics.lineTo(x,endy);
      grid.graphics.lineTo(x,starty);
    }
    for(y = starty;y<=endy;y+=delta){
      grid.graphics.lineTo(startx,y);
      grid.graphics.lineTo(endx,y);
      grid.graphics.lineTo(startx,y);
    }
    return grid;
  }
  function updateGrid(){
    var dx = world.regX*scalingFactor %gridSize;
    var dy = world.regY*scalingFactor %gridSize;
    grid.x = -dx;
    grid.y = -dy;
  }

  function setCameraTarget(targetId){
    cameraTarget = targetId;
  }
  function moveCamera(actorPos){
    var xoffset = canvas.width/(2*scalingFactor);
    var yoffset = canvas.height/(2*scalingFactor);

    var max_dx = canvas.width/(6*scalingFactor);
    var max_dy = canvas.height/(6*scalingFactor);
    var dx = world.regX-(actorPos[0]-xoffset);
    var dy = world.regY-(actorPos[1]-yoffset);


    if(dx > max_dx){
      world.regX = actorPos[0]+max_dx-xoffset;
    }
    if(dx < -max_dx){
      world.regX = actorPos[0]-max_dx-xoffset;
    }
    if(dy > max_dy){
      world.regY = actorPos[1]+max_dy-yoffset;
    }
    if(dy < -max_dy){
      world.regY = actorPos[1]-max_dy-yoffset;
    }

    updateGrid();
  }
  function centerCamera(actorPos){
    world.regX = actorPos[0]-canvas.width/(2*scalingFactor);
    world.regY = actorPos[1]-canvas.height/(2*scalingFactor);
  }



  function init(){
    canvas = options.canvasElement;
    stage = new createjs.Stage(canvas);
    world = new createjs.Container();
    canvasUI = new PylonClient.CanvasUI().init();
    world.x = 0;
    world.y =0;
    stage.addChild(world);
    window.addEventListener('resize', resizeToWindow, false);
    resizeToWindow();
    return that;
  }

  that.init = init;
  that.update = update;
  that.setCameraTarget = setCameraTarget;
  that.reset = reset;
  that.clear = clear;
  return that;
};