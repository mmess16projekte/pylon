/* global PylonClient, createjs */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.CanvasUI
 * @description EaselJS Container that contains all UI related elements
 *    User Messages, Health Energy and Repuation bars. Receives "meta" Events
 *    and draws them.
 */
PylonClient.CanvasUI = function(){
  var that = {
    container:null,
    width:0,
    height:0,
    data:null
  };
 


  var healthBar;
  var frame1;
  var energyBar;
  var frame2;
  var repuationBars;
  var repuationFrames;
  var repuationTexts;
  var repuationLabel;
  var factionLabel;
  var factionCircle;
  var messageLabel;




  function init(){
    var i,bar,frame,text;
    that.container = new createjs.Container();
    healthBar = new createjs.Shape();
    that.container.addChild(healthBar);
    frame1 = new createjs.Shape();
    that.container.addChild(frame1);

    energyBar = new createjs.Shape();
    that.container.addChild(energyBar);
    frame2 = new createjs.Shape();
    that.container.addChild(frame2);

    repuationBars =[];
    repuationFrames = [];
    repuationTexts = [];
    for(i=0;i<3;i++){
      bar = new createjs.Shape();
      repuationBars.push(bar);
      that.container.addChild(bar);
      frame = new createjs.Shape();
      repuationFrames.push(frame);
      that.container.addChild(frame);
      text = new createjs.Text();
      text.set({
        font: '14px Arial',
        text: '',
        textAlign: 'center',
        color:'#000'
      });
      repuationTexts.push(text);
      that.container.addChild(text);
    }
    repuationLabel = new createjs.Text();
    repuationLabel.set({
      font: '15px Arial',
      text: 'Reputation:',
      textAlign: 'left',
      color:'#000'
    });
    that.container.addChild(repuationLabel);
    factionLabel = new createjs.Text();
    factionLabel.set({
      font: '15px Arial',
      text: 'Current Faction:',
      textAlign: 'center',
      color:'#000'
    });
    that.container.addChild(factionLabel);
    factionCircle = new createjs.Shape();
    that.container.addChild(factionCircle);

    messageLabel = new createjs.Text();
    messageLabel.set({
      font: '20px Arial',
      text: 'test text',
      textAlign: 'center',
      color:'#777'
    });
    that.container.addChild(messageLabel);


    return that;
  }

  function redraw(){
    if(!that.data){
      return;
    }
    drawHealthbar();
    drawEnergybar();
    drawRepuationBars();
    drawMessages();
  }

  function drawHealthbar(){
    var x,y,width,height;

    width = 350*that.data.health / that.data.max_health;
    height = 6;
    x = that.width - 350 -20;
    y = that.height - height -40;

    healthBar.graphics.clear();
    if(width > 0){
      healthBar.graphics
        .beginFill('#66FF00')
        .drawRoundRect(x,y,width,height,2);
    }
    frame1.graphics
      .clear()
      .beginStroke('#000')
      .drawRoundRect(x-2,y-2,350+4,height+4,3);

  }
  function drawEnergybar(){
    var x,y,width,height;

    width = 350*that.data.energy / that.data.max_energy;
    height = 6;
    x = that.width - 350 -20;
    y = that.height - height -20;

    energyBar.graphics.clear();
    if(width > 0){
      energyBar.graphics
        .beginFill('#add8e6')
        .drawRoundRect(x,y,width,height,2); 
    }
    energyBar.graphics
      .clear()
      .beginFill('#add8e6')
      .drawRoundRect(x,y,width,height,2);
    frame2.graphics
      .clear()
      .beginStroke('#000')
      .drawRoundRect(x-2,y-2,350+4,height+4,3);

  }


  function drawRepuationBars(){
    var i,bar,frame,rep,text;
    var x,y,width,height,color;
    var factions = ['red','blue','yellow'];
    var colors = {
      red:'#F00',
      blue:'#00F',
      yellow:'#CC2',
      white:'#CCC'
    };
    height = 4;
    x = 20;


    for(i=0;i<3;i++){
      bar = repuationBars[i];
      frame = repuationFrames[i];
      text = repuationTexts[i];
      rep = that.data.rep[factions[i]];
      y = that.height - i*(height+12)-20;
      width = 200*rep.v/rep.max;
      bar.graphics.clear();
      bar.graphics
        .beginFill(colors[factions[i]])
        .drawRoundRect(x,y,width,height,2); 
      frame.graphics
        .clear()
        .beginStroke('#000')
        .drawRoundRect(x-2,y-2,200+4,height+4,3);
      text.set({
        text:'lvl: '+rep.l,
        color:colors[factions[i]]
      });
      text.x = x+220;
      text.y = y-8;
    }
    color = colors[that.data.rep.maxFaction] || '#CCC';
    factionCircle.graphics.beginFill(color).drawCircle(0, 0, 20);
    factionCircle.x = x+280;
    factionCircle.y = that.height - 35;

    repuationLabel.x = x;
    repuationLabel.y = that.height - 75; 
    factionLabel.x = x+280;
    factionLabel.y = that.height - 75;
  }

  function drawMessages(){
    var text = '';
    messageLabel.x = that.width /2;
    messageLabel.y = that.height * 5/6;
    if(that.data.messages.length){
      text = that.data.messages.join('\n');
    }
    messageLabel.set({
      text:text
    });

  }

  




  function resize(width,height){
    that.width = width;
    that.height = height;
    redraw();
  }
  function update(data){
    that.data = data;
    redraw();
  }
 


  that.init = init;
  that.resize = resize;
  that.update = update;
  return that;
};