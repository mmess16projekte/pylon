/**
 * @namespace PylonClient
 * @function PylonClient
 * @description Application entry point, initializes all other client modules. 
 */

var PylonClient = function(){
  var that = {};
  var resourceLoader;
  var canvas;
  var webSocket;
  var staticData;
  var keyboardListener;
  var inputHandler;
  var deserializer;
  var modalController;
  var interpolator;

  var resources;
  

 

  function onInput(event){
    var input = event.data;
    webSocket.sendInput(input.action,input.state);
  }
  function onStateUpdate(event){
    canvas.update(event.data);
  }
  function onSocketReady(){
    modalController.openNameSelectModal();
    canvas.setCameraTarget(webSocket.getId());
    canvas.reset();
  }
  function onDisconnect(){
    canvas.clear();
  }
  function onNameSelected(event){
    var name = event.data;
    webSocket.login({
      name:name
    });
    keyboardListener.enable();
  }
  function onPlayerDeath(event){
    var message = event.data.message || '';
    keyboardListener.disable();
    modalController.openDeathAlert(message);
  }

  function onUpdate(event){
    deserializer.update(event.data);
  }

  function onResourcesReady(event){
    resources = event.data;
    initGame();
  }

  function initGame(){
    keyboardListener = new PylonClient.KeyboardListener({

    }).init();
    inputHandler = new PylonClient.InputHandler({
      keyboardListener:keyboardListener
    }).init();
    inputHandler.addEventListener('input',onInput);


    staticData = new PylonClient.StaticData({
      data:resources.jsonData
    }).init();
    interpolator = new PylonClient.Interpolator({

    }).init();
    deserializer = new PylonClient.Deserializer({
      staticData:staticData,
      interpolator:interpolator
    }).init();
    deserializer.addEventListener('stateUpdate',onStateUpdate);
    canvas = new PylonClient.Canvas({
      canvasElement:document.querySelector('#maincanvas')
    }).init();
    modalController = new PylonClient.ModalController({

    }).init();
    modalController.addEventListener('nameSelected',onNameSelected);
    webSocket = new PylonClient.WebSocket({
    });
    webSocket.addEventListener('ready',onSocketReady);
    webSocket.addEventListener('disconnect',onDisconnect);
    webSocket.addEventListener('update',onUpdate);
    webSocket.addEventListener('death',onPlayerDeath);
    webSocket.init();
  }



  function init(){
    resourceLoader = new PylonClient.ResourceLoader({

    }).init();
    resourceLoader.addEventListener('resourcesReady',onResourcesReady);
    resourceLoader.load();
  }



  that.init = init;
  return that;
};  