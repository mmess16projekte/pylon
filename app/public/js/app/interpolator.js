/* global PylonClient */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.Interpolator
 * @description Used by the Deserializer, an actor may indicate that its 
 *    movement can be interpolated without the need for the server to send 
 *    constant updates. This module creates so called interpolated Events for 
 *    these actors that look like updates sent from the Server and that can be
 *    drawn by the canvas. 
 *    This is used e.g for projectiles since the move in one direction with 
 *    constant speed, and it drastically reduces server load. 
 */
PylonClient.Interpolator = function(){
  var that = {};

  var actorIndex;
 
  function init(){
    actorIndex = {};   
    return that;
  }

  function addActor(actor){
    actorIndex[actor.id] = actor;
  }
  function removeActor(actor){
    delete actorIndex[actor.id];
  }

  function interpolateActor(actor,dt){
    actor.p[0] += actor.v[0]*dt;
    actor.p[1] += actor.v[1]*dt;
  }

  function getInterpolatedEvents(dt){
    var ipEvents = [];
    var actor;
    Object.keys(actorIndex).forEach(function(actorId){
      actor = actorIndex[actorId];
      interpolateActor(actor,dt);
      ipEvents.push({
        type:'update',
        actor:actor
      });
    });
    return ipEvents;
  }

 
  that.init = init;
  that.addActor = addActor;
  that.removeActor = removeActor;
  that.getInterpolatedEvents = getInterpolatedEvents;
  
  return that;
};