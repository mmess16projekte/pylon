/* global PylonClient, EventPublisher, vex, $ */
/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.ModalController
 * @description Responsible for presenting modals, calls the vex library. 
 */
PylonClient.ModalController = function(){
  var that = new EventPublisher();

  function init(){
    vex.defaultOptions.className = 'vex-theme-flat-attack';
    return that;
  }
  function openDeathAlert(message){
    vex.dialog.alert({
      message:'You died. Cause:'+message,
      callback:function(){
        openNameSelectModal();
      }
    });
  }


  function openNameHelpAlert(){
    vex.dialog.alert({
      message:'Please choose an alphanumeric username of length 2-10',
      callback:function(){
        openNameSelectModal();
      }
    });

  }


  function openNameSelectModal(){
    

    vex.dialog.open({
      message: 'Please choose a name:',
      input: '<input name=\"username\" type=\"text\" placeholder=\"Username\" maxlength=\"10\" />\n',
      buttons: [
        $.extend({}, vex.dialog.buttons.YES, {
          text: 'START'
        }), $.extend({}, vex.dialog.buttons.NO, {
          text: 'Back'
        })
      ],
      callback: function(data) {
        var regexp = new RegExp('(?=^.{1,10}$)^[a-zA-Z][a-zA-Z0-9]*[._-]?[a-zA-Z0-9]+$');
        if (data === false) {
          openNameSelectModal();
          return;
        }
        if(data.username === ''){
          data.username = 'unnamed';
        }
        if(regexp.test(data.username)){
          that.notifyAll('nameSelected',data.username);
        } else {
          openNameHelpAlert();
        }
      }
    });
  }



  that.init = init;
  that.openNameSelectModal = openNameSelectModal;
  that.openDeathAlert = openDeathAlert;
  return that;
};