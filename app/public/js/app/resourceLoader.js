/* global PylonClient, EventPublisher, $ */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.ResourceLoader
 * @description Loads static data from the server, this sattic data contains
 *    the shape and color of all actors and other parameters necessary for 
 *    drawing.
 */
PylonClient.ResourceLoader = function(){
  var that = new EventPublisher();
  var data;

  function init(){
    data = {};
    return that;
  }

  function onJSON(jsonData){
    data.jsonData = jsonData;
    that.notifyAll('resourcesReady',data);
  }

  function loadJSON(){
    var url = '/js/dist/gameData.json';
    $.getJSON(url,onJSON);
  }
  function load(){
    loadJSON();
  }


  that.init = init;
  that.load = load;
  return that;
};