/* global PylonClient */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.KeyboardListener
 * @description Adds listeners to the document for keyboard events and passes 
 *    them to the input handler.
 */
PylonClient.KeyboardListener = function(){
  var that = {};
  
  var states = {
    up:0,
    down:1
  };
  var enabled;

  var listeners ={};

  function init(){
    enabled = false;
    document.onkeydown = function (e) {
      var event = e || window.event;
      if(listeners[event.keyCode] && enabled){
        listeners[event.keyCode](states.down);
        event.preventDefault();
      }
    };
    document.onkeyup = function(e){
      var event = e || window.event;
      if(listeners[event.keyCode] && enabled){
        listeners[event.keyCode](states.up);
        event.preventDefault();
      }
    };
    return that;
  }
  function addKeyCodeListener(keycode,listener){
    listeners[keycode]=listener;
  }
  function disable(){
    enabled = false;
  }
  function enable(){
    enabled = true;
  }


  that.init = init;
  that.addKeyCodeListener = addKeyCodeListener;
  that.disable = disable;
  that.enable = enable;
  return that;
  
};