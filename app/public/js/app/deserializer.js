/* global PylonClient, EventPublisher */
/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.Deserializer
 * @description Receives packages from the websocket. This module 
 *    handles different types of packages.
 *    - create packages contain all necessary state for drawing and are sent 
 *      once for each actor
 *    - update packages contain only changed values, this module maps them to 
 *      the full state and send it on to the canvas. 
 *    - destroy packages tell it to discard an actor and it tells the canvas to
 *      delete it.
 *    - meta packages contain information for the user and get handled like 
 *      update packages and sent to the canvas. 
 */
PylonClient.Deserializer = function(options){
  var that = new EventPublisher();
  var staticData;
  var interpolator;
  var state = {
    actors:{},
    meta:{}
  };
  var events;
 

  function handlePackage(package){
    var type = package.t;
    delete package.t;
    switch(type){
    case 1: // create
      createActor(package);
      break;
    case 2: // update
      updateActor(package);
      break;
    case 3: // destroy
      destroyActor(package);
      break;
    case 4: // meta
      updateMeta(package);
      break;
    case 5: //ignore
      break;
    default:
      console.error('Unhandled type:'+package.t);
    }
  }
  function createActor(package){
    package.staticData = staticData.load(package.an);
    if(state.actors[package.id]){
      updateActor(package);
      return;
    }
    state.actors[package.id]= package;
    if(package.ip){
      interpolator.addActor(package);
    }
    if(package.path){
      package.staticData.path = package.path;
    }
    events.push({
      type:'create',
      actor:package
    });
    events.push({
      type:'update',
      actor:package
    });
  }

  function updateActor(package){
    var actor = state.actors[package.id];
    Object.keys(package).forEach(function(key){
      actor[key] = package[key];
    });
    events.push({
      type:'update',
      actor:actor
    });
  }
  function destroyActor(package){
    var actor = state.actors[package.id];
    if(actor.ip){
      interpolator.removeActor(actor);
    }
    delete state.actors[package.id];
    events.push({
      type:'destroy',
      actor:{
        id:package.id
      }
    });
  }
  function updateMeta(package){
    Object.keys(package).forEach(function(key){
      state.meta[key] = package[key];
    });
    events.push({
      type:'meta',
      meta:state.meta
    });
  }

  function update(data){
    var deltaTime = data.global.dt || 1/30;
    var ipEvents;
    data.frame.forEach(handlePackage);
    ipEvents = interpolator.getInterpolatedEvents(deltaTime);
    events = events.concat(ipEvents);
    that.notifyAll('stateUpdate',events);
    events = [];
  }
  function init(){
    staticData = options.staticData;
    interpolator = options.interpolator;
    events = [];
    return that;
  }
  

  that.init = init;
  that.update = update;
  return that;
};