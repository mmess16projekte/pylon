/**
 * @namespace PylonClient
 * @function PylonClient
 * @description Application entry point, initializes all other client modules. 
 */

var PylonClient = function(){
  var that = {};
  var resourceLoader;
  var canvas;
  var webSocket;
  var staticData;
  var keyboardListener;
  var inputHandler;
  var deserializer;
  var modalController;
  var interpolator;

  var resources;
  

 

  function onInput(event){
    var input = event.data;
    webSocket.sendInput(input.action,input.state);
  }
  function onStateUpdate(event){
    canvas.update(event.data);
  }
  function onSocketReady(){
    modalController.openNameSelectModal();
    canvas.setCameraTarget(webSocket.getId());
    canvas.reset();
  }
  function onDisconnect(){
    canvas.clear();
  }
  function onNameSelected(event){
    var name = event.data;
    webSocket.login({
      name:name
    });
    keyboardListener.enable();
  }
  function onPlayerDeath(event){
    var message = event.data.message || '';
    keyboardListener.disable();
    modalController.openDeathAlert(message);
  }

  function onUpdate(event){
    deserializer.update(event.data);
  }

  function onResourcesReady(event){
    resources = event.data;
    initGame();
  }

  function initGame(){
    keyboardListener = new PylonClient.KeyboardListener({

    }).init();
    inputHandler = new PylonClient.InputHandler({
      keyboardListener:keyboardListener
    }).init();
    inputHandler.addEventListener('input',onInput);


    staticData = new PylonClient.StaticData({
      data:resources.jsonData
    }).init();
    interpolator = new PylonClient.Interpolator({

    }).init();
    deserializer = new PylonClient.Deserializer({
      staticData:staticData,
      interpolator:interpolator
    }).init();
    deserializer.addEventListener('stateUpdate',onStateUpdate);
    canvas = new PylonClient.Canvas({
      canvasElement:document.querySelector('#maincanvas')
    }).init();
    modalController = new PylonClient.ModalController({

    }).init();
    modalController.addEventListener('nameSelected',onNameSelected);
    webSocket = new PylonClient.WebSocket({
    });
    webSocket.addEventListener('ready',onSocketReady);
    webSocket.addEventListener('disconnect',onDisconnect);
    webSocket.addEventListener('update',onUpdate);
    webSocket.addEventListener('death',onPlayerDeath);
    webSocket.init();
  }



  function init(){
    resourceLoader = new PylonClient.ResourceLoader({

    }).init();
    resourceLoader.addEventListener('resourcesReady',onResourcesReady);
    resourceLoader.load();
  }



  that.init = init;
  return that;
};  
/* global PylonClient, createjs */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.CanvasUI
 * @description EaselJS Container that contains all UI related elements
 *    User Messages, Health Energy and Repuation bars. Receives "meta" Events
 *    and draws them.
 */
PylonClient.CanvasUI = function(){
  var that = {
    container:null,
    width:0,
    height:0,
    data:null
  };
 


  var healthBar;
  var frame1;
  var energyBar;
  var frame2;
  var repuationBars;
  var repuationFrames;
  var repuationTexts;
  var repuationLabel;
  var factionLabel;
  var factionCircle;
  var messageLabel;




  function init(){
    var i,bar,frame,text;
    that.container = new createjs.Container();
    healthBar = new createjs.Shape();
    that.container.addChild(healthBar);
    frame1 = new createjs.Shape();
    that.container.addChild(frame1);

    energyBar = new createjs.Shape();
    that.container.addChild(energyBar);
    frame2 = new createjs.Shape();
    that.container.addChild(frame2);

    repuationBars =[];
    repuationFrames = [];
    repuationTexts = [];
    for(i=0;i<3;i++){
      bar = new createjs.Shape();
      repuationBars.push(bar);
      that.container.addChild(bar);
      frame = new createjs.Shape();
      repuationFrames.push(frame);
      that.container.addChild(frame);
      text = new createjs.Text();
      text.set({
        font: '14px Arial',
        text: '',
        textAlign: 'center',
        color:'#000'
      });
      repuationTexts.push(text);
      that.container.addChild(text);
    }
    repuationLabel = new createjs.Text();
    repuationLabel.set({
      font: '15px Arial',
      text: 'Reputation:',
      textAlign: 'left',
      color:'#000'
    });
    that.container.addChild(repuationLabel);
    factionLabel = new createjs.Text();
    factionLabel.set({
      font: '15px Arial',
      text: 'Current Faction:',
      textAlign: 'center',
      color:'#000'
    });
    that.container.addChild(factionLabel);
    factionCircle = new createjs.Shape();
    that.container.addChild(factionCircle);

    messageLabel = new createjs.Text();
    messageLabel.set({
      font: '20px Arial',
      text: 'test text',
      textAlign: 'center',
      color:'#777'
    });
    that.container.addChild(messageLabel);


    return that;
  }

  function redraw(){
    if(!that.data){
      return;
    }
    drawHealthbar();
    drawEnergybar();
    drawRepuationBars();
    drawMessages();
  }

  function drawHealthbar(){
    var x,y,width,height;

    width = 350*that.data.health / that.data.max_health;
    height = 6;
    x = that.width - 350 -20;
    y = that.height - height -40;

    healthBar.graphics.clear();
    if(width > 0){
      healthBar.graphics
        .beginFill('#66FF00')
        .drawRoundRect(x,y,width,height,2);
    }
    frame1.graphics
      .clear()
      .beginStroke('#000')
      .drawRoundRect(x-2,y-2,350+4,height+4,3);

  }
  function drawEnergybar(){
    var x,y,width,height;

    width = 350*that.data.energy / that.data.max_energy;
    height = 6;
    x = that.width - 350 -20;
    y = that.height - height -20;

    energyBar.graphics.clear();
    if(width > 0){
      energyBar.graphics
        .beginFill('#add8e6')
        .drawRoundRect(x,y,width,height,2); 
    }
    energyBar.graphics
      .clear()
      .beginFill('#add8e6')
      .drawRoundRect(x,y,width,height,2);
    frame2.graphics
      .clear()
      .beginStroke('#000')
      .drawRoundRect(x-2,y-2,350+4,height+4,3);

  }


  function drawRepuationBars(){
    var i,bar,frame,rep,text;
    var x,y,width,height,color;
    var factions = ['red','blue','yellow'];
    var colors = {
      red:'#F00',
      blue:'#00F',
      yellow:'#CC2',
      white:'#CCC'
    };
    height = 4;
    x = 20;


    for(i=0;i<3;i++){
      bar = repuationBars[i];
      frame = repuationFrames[i];
      text = repuationTexts[i];
      rep = that.data.rep[factions[i]];
      y = that.height - i*(height+12)-20;
      width = 200*rep.v/rep.max;
      bar.graphics.clear();
      bar.graphics
        .beginFill(colors[factions[i]])
        .drawRoundRect(x,y,width,height,2); 
      frame.graphics
        .clear()
        .beginStroke('#000')
        .drawRoundRect(x-2,y-2,200+4,height+4,3);
      text.set({
        text:'lvl: '+rep.l,
        color:colors[factions[i]]
      });
      text.x = x+220;
      text.y = y-8;
    }
    color = colors[that.data.rep.maxFaction] || '#CCC';
    factionCircle.graphics.beginFill(color).drawCircle(0, 0, 20);
    factionCircle.x = x+280;
    factionCircle.y = that.height - 35;

    repuationLabel.x = x;
    repuationLabel.y = that.height - 75; 
    factionLabel.x = x+280;
    factionLabel.y = that.height - 75;
  }

  function drawMessages(){
    var text = '';
    messageLabel.x = that.width /2;
    messageLabel.y = that.height * 5/6;
    if(that.data.messages.length){
      text = that.data.messages.join('\n');
    }
    messageLabel.set({
      text:text
    });

  }

  




  function resize(width,height){
    that.width = width;
    that.height = height;
    redraw();
  }
  function update(data){
    that.data = data;
    redraw();
  }
 


  that.init = init;
  that.resize = resize;
  that.update = update;
  return that;
};
/* global PylonClient, createjs */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.Canvas
 * @description Responsible for drawing, interfaces with EaselJS. 
 *    Uses a moving container to draw all world objects and to simulate a moving
 *    camera. Passes "meta" events to the UI canvas component. 
 */
PylonClient.Canvas = function(options){
  var that = {};
  var canvas;
  var scalingFactor;
  var gridSize = 80;
  var canvasUI;

  
  var stage;
  var world;
  var actorShapes = {};
  var grid;

  //var camera = [0,0];
  var cameraTarget;


  function resizeToWindow(){
    var width = window.innerWidth;
    var height = window.innerHeight;
   

    var desiredAspectRatio = 16/9;
    var windowAspectRatio = width/height;

    if(windowAspectRatio > desiredAspectRatio){
      width = height*desiredAspectRatio;
    } else {
      height = width/desiredAspectRatio;
    }
    canvas.width = width;
    canvas.height = height;

    scalingFactor = (width/1440)*8;
    world.scaleX=scalingFactor;
    world.scaleY=scalingFactor;
    
    canvas.style.marginTop = (-height/2)+'px';
    canvas.style.marginLeft = (-width/2)+'px';
    

    canvasUI.resize(width,height);
    reset();
  }

  function reset(){
    stage.removeAllChildren();
    world.removeAllChildren();
    stage.clear();
    stage.addChild(world);
    stage.addChild(canvasUI.container);
    Object.keys(actorShapes).forEach(function(id){
      addShapeToWorld(actorShapes[id]);
    });
    grid = createGrid();
    stage.addChild(grid);
    stage.update();
  }
  function clear(){
    actorShapes = {};
    reset();
  }

  function update(eventList){
    var events = eventList || [];
    var shape;
    var actor;
    events.forEach(function(event){
      actor = event.actor;
      switch(event.type){
      case 'create':
        shape = createShape(actor);
        actorShapes[actor.id] = shape;
        addShapeToWorld(shape);
        break;
      case 'update':
        shape = actorShapes[actor.id];
        updateShape(shape,actor);
        break;
      case 'destroy':
        shape = actorShapes[actor.id];
        removeShape(shape,actor);
        break;
      case 'meta':
        canvasUI.update(event.meta);
        break;
      default:
        break;
      }
    });
    stage.update();
  }
  function addShapeToWorld(shape){
    world.addChild(shape);
    if(shape.label){
      world.addChild(shape.label);
    }
  }

  function createShape(actor){
    var com = actor.com;
    var path = actor.staticData.path;
    var text = actor.staticData.text;
    var shape;
    if(text){
      shape = createText(text,com,actor.staticData);
    } else{
      shape = createPolygon(path,com,actor.staticData);
    }
    if(actor.name){
      shape.label = createLabel(actor.name);
    }
    if(actor.cid === cameraTarget){
      centerCamera(actor.p);
    }
    return shape;
  }
  function updateShape(shape,actor){
    var pos = actor.p;
    var angle = actor.a;
    var degrees = angle * 180 / Math.PI;
    shape.x = pos[0];
    shape.y = pos[1];
    shape.rotation = degrees;
    if(shape.label){
      shape.label.x = shape.x;
      shape.label.y = shape.y+6;
    }
    if(actor.cid === cameraTarget){
      moveCamera(actor.p);
    }
  }
  function removeShape(shape,actor){
    world.removeChild(shape);
    if(shape.label){
      world.removeChild(shape.label);
    }
    delete actorShapes[actor.id];

  }

  function createPolygon(path,center,options){
    var polygon = new createjs.Shape();
    var i;
    var fillColor = options.fillColor || 'red';
    var strokeColor = options.strokeColor || 'black';
    polygon.graphics.setStrokeStyle(0.1).beginFill(fillColor).beginStroke(strokeColor);
    polygon.graphics.moveTo(path[0][0],path[0][1]);
    for(i=1;i<path.length;i++){
      polygon.graphics.lineTo(path[i][0],path[i][1]);
    }
    polygon.graphics.closePath();
    polygon.regX = center[0];
    polygon.regY = center[1];
    return polygon;
  }
  function createLabel(name){
    var label = new createjs.Text();
    label.set({
      font: '2px Arial',
      text: name,
      textAlign: 'center'
    });
    return label;
  }

  function createText(text,center,options){
    var fillColor = options.fillColor || 'red';
    var size = options.textSize || '3px';
    var font = 'Arial';
    var textShape = new createjs.Text();
    textShape.set({
      font: size+' '+font,
      text: text,
      textAlign: 'center',
      color:fillColor
    });
    textShape.regX = center[0];
    textShape.regY = center[1];
    return textShape;
  }

  function createGrid(){
    var grid = new createjs.Shape();
    var startx = -100;
    var endx = canvas.width+100;
    var starty = -100;
    var endy = canvas.height+100;
    var delta = gridSize;
    var x;
    var y;

    grid.graphics.setStrokeStyle(0.2);
    grid.graphics.beginStroke('#CCC');
    grid.graphics.moveTo(startx,starty);
    for(x = startx;x<=endx;x+=delta){
      grid.graphics.lineTo(x,starty);
      grid.graphics.lineTo(x,endy);
      grid.graphics.lineTo(x,starty);
    }
    for(y = starty;y<=endy;y+=delta){
      grid.graphics.lineTo(startx,y);
      grid.graphics.lineTo(endx,y);
      grid.graphics.lineTo(startx,y);
    }
    return grid;
  }
  function updateGrid(){
    var dx = world.regX*scalingFactor %gridSize;
    var dy = world.regY*scalingFactor %gridSize;
    grid.x = -dx;
    grid.y = -dy;
  }

  function setCameraTarget(targetId){
    cameraTarget = targetId;
  }
  function moveCamera(actorPos){
    var xoffset = canvas.width/(2*scalingFactor);
    var yoffset = canvas.height/(2*scalingFactor);

    var max_dx = canvas.width/(6*scalingFactor);
    var max_dy = canvas.height/(6*scalingFactor);
    var dx = world.regX-(actorPos[0]-xoffset);
    var dy = world.regY-(actorPos[1]-yoffset);


    if(dx > max_dx){
      world.regX = actorPos[0]+max_dx-xoffset;
    }
    if(dx < -max_dx){
      world.regX = actorPos[0]-max_dx-xoffset;
    }
    if(dy > max_dy){
      world.regY = actorPos[1]+max_dy-yoffset;
    }
    if(dy < -max_dy){
      world.regY = actorPos[1]-max_dy-yoffset;
    }

    updateGrid();
  }
  function centerCamera(actorPos){
    world.regX = actorPos[0]-canvas.width/(2*scalingFactor);
    world.regY = actorPos[1]-canvas.height/(2*scalingFactor);
  }



  function init(){
    canvas = options.canvasElement;
    stage = new createjs.Stage(canvas);
    world = new createjs.Container();
    canvasUI = new PylonClient.CanvasUI().init();
    world.x = 0;
    world.y =0;
    stage.addChild(world);
    window.addEventListener('resize', resizeToWindow, false);
    resizeToWindow();
    return that;
  }

  that.init = init;
  that.update = update;
  that.setCameraTarget = setCameraTarget;
  that.reset = reset;
  that.clear = clear;
  return that;
};
/* global PylonClient, EventPublisher */
/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.Deserializer
 * @description Receives packages from the websocket. This module 
 *    handles different types of packages.
 *    - create packages contain all necessary state for drawing and are sent 
 *      once for each actor
 *    - update packages contain only changed values, this module maps them to 
 *      the full state and send it on to the canvas. 
 *    - destroy packages tell it to discard an actor and it tells the canvas to
 *      delete it.
 *    - meta packages contain information for the user and get handled like 
 *      update packages and sent to the canvas. 
 */
PylonClient.Deserializer = function(options){
  var that = new EventPublisher();
  var staticData;
  var interpolator;
  var state = {
    actors:{},
    meta:{}
  };
  var events;
 

  function handlePackage(package){
    var type = package.t;
    delete package.t;
    switch(type){
    case 1: // create
      createActor(package);
      break;
    case 2: // update
      updateActor(package);
      break;
    case 3: // destroy
      destroyActor(package);
      break;
    case 4: // meta
      updateMeta(package);
      break;
    case 5: //ignore
      break;
    default:
      console.error('Unhandled type:'+package.t);
    }
  }
  function createActor(package){
    package.staticData = staticData.load(package.an);
    if(state.actors[package.id]){
      updateActor(package);
      return;
    }
    state.actors[package.id]= package;
    if(package.ip){
      interpolator.addActor(package);
    }
    if(package.path){
      package.staticData.path = package.path;
    }
    events.push({
      type:'create',
      actor:package
    });
    events.push({
      type:'update',
      actor:package
    });
  }

  function updateActor(package){
    var actor = state.actors[package.id];
    Object.keys(package).forEach(function(key){
      actor[key] = package[key];
    });
    events.push({
      type:'update',
      actor:actor
    });
  }
  function destroyActor(package){
    var actor = state.actors[package.id];
    if(actor.ip){
      interpolator.removeActor(actor);
    }
    delete state.actors[package.id];
    events.push({
      type:'destroy',
      actor:{
        id:package.id
      }
    });
  }
  function updateMeta(package){
    Object.keys(package).forEach(function(key){
      state.meta[key] = package[key];
    });
    events.push({
      type:'meta',
      meta:state.meta
    });
  }

  function update(data){
    var deltaTime = data.global.dt || 1/30;
    var ipEvents;
    data.frame.forEach(handlePackage);
    ipEvents = interpolator.getInterpolatedEvents(deltaTime);
    events = events.concat(ipEvents);
    that.notifyAll('stateUpdate',events);
    events = [];
  }
  function init(){
    staticData = options.staticData;
    interpolator = options.interpolator;
    events = [];
    return that;
  }
  

  that.init = init;
  that.update = update;
  return that;
};
/* global PylonClient, EventPublisher */
/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.InputHandler
 * @description Maps keyboard inputs to actions that get sent to the server. 
 */
PylonClient.InputHandler = function(options){
  var that = new EventPublisher();
  var keyboardListener;

  var keyCodes = {
    38:'up',
    87:'up',
    37:'left',
    65:'left',
    39:'right',
    68:'right',
    40:'down',
    83:'down',
    69:'fire',
    32:'fire'
  };
  var currentState={
  };

  function initListeners(){
    Object.keys(keyCodes).forEach(function(code){
      keyboardListener.addKeyCodeListener(code,function(state){
        var action = keyCodes[code];
        if(currentState[action] == state){
          return;
        }
        currentState[action] = state;
        that.notifyAll('input',{
          action:action,
          state:state
        });
      });
    });
  }
 
  function init(){
    keyboardListener = options.keyboardListener;
    initListeners();
    return that;
  }
  that.init = init;
  return that;
};
/* global PylonClient */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.Interpolator
 * @description Used by the Deserializer, an actor may indicate that its 
 *    movement can be interpolated without the need for the server to send 
 *    constant updates. This module creates so called interpolated Events for 
 *    these actors that look like updates sent from the Server and that can be
 *    drawn by the canvas. 
 *    This is used e.g for projectiles since the move in one direction with 
 *    constant speed, and it drastically reduces server load. 
 */
PylonClient.Interpolator = function(){
  var that = {};

  var actorIndex;
 
  function init(){
    actorIndex = {};   
    return that;
  }

  function addActor(actor){
    actorIndex[actor.id] = actor;
  }
  function removeActor(actor){
    delete actorIndex[actor.id];
  }

  function interpolateActor(actor,dt){
    actor.p[0] += actor.v[0]*dt;
    actor.p[1] += actor.v[1]*dt;
  }

  function getInterpolatedEvents(dt){
    var ipEvents = [];
    var actor;
    Object.keys(actorIndex).forEach(function(actorId){
      actor = actorIndex[actorId];
      interpolateActor(actor,dt);
      ipEvents.push({
        type:'update',
        actor:actor
      });
    });
    return ipEvents;
  }

 
  that.init = init;
  that.addActor = addActor;
  that.removeActor = removeActor;
  that.getInterpolatedEvents = getInterpolatedEvents;
  
  return that;
};
/* global PylonClient */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.KeyboardListener
 * @description Adds listeners to the document for keyboard events and passes 
 *    them to the input handler.
 */
PylonClient.KeyboardListener = function(){
  var that = {};
  
  var states = {
    up:0,
    down:1
  };
  var enabled;

  var listeners ={};

  function init(){
    enabled = false;
    document.onkeydown = function (e) {
      var event = e || window.event;
      if(listeners[event.keyCode] && enabled){
        listeners[event.keyCode](states.down);
        event.preventDefault();
      }
    };
    document.onkeyup = function(e){
      var event = e || window.event;
      if(listeners[event.keyCode] && enabled){
        listeners[event.keyCode](states.up);
        event.preventDefault();
      }
    };
    return that;
  }
  function addKeyCodeListener(keycode,listener){
    listeners[keycode]=listener;
  }
  function disable(){
    enabled = false;
  }
  function enable(){
    enabled = true;
  }


  that.init = init;
  that.addKeyCodeListener = addKeyCodeListener;
  that.disable = disable;
  that.enable = enable;
  return that;
  
};
/* global PylonClient, EventPublisher, vex, $ */
/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.ModalController
 * @description Responsible for presenting modals, calls the vex library. 
 */
PylonClient.ModalController = function(){
  var that = new EventPublisher();

  function init(){
    vex.defaultOptions.className = 'vex-theme-flat-attack';
    return that;
  }
  function openDeathAlert(message){
    vex.dialog.alert({
      message:'You died. Cause:'+message,
      callback:function(){
        openNameSelectModal();
      }
    });
  }


  function openNameHelpAlert(){
    vex.dialog.alert({
      message:'Please choose an alphanumeric username of length 2-10',
      callback:function(){
        openNameSelectModal();
      }
    });

  }


  function openNameSelectModal(){
    

    vex.dialog.open({
      message: 'Please choose a name:',
      input: '<input name=\"username\" type=\"text\" placeholder=\"Username\" maxlength=\"10\" />\n',
      buttons: [
        $.extend({}, vex.dialog.buttons.YES, {
          text: 'START'
        }), $.extend({}, vex.dialog.buttons.NO, {
          text: 'Back'
        })
      ],
      callback: function(data) {
        var regexp = new RegExp('(?=^.{1,10}$)^[a-zA-Z][a-zA-Z0-9]*[._-]?[a-zA-Z0-9]+$');
        if (data === false) {
          openNameSelectModal();
          return;
        }
        if(data.username === ''){
          data.username = 'unnamed';
        }
        if(regexp.test(data.username)){
          that.notifyAll('nameSelected',data.username);
        } else {
          openNameHelpAlert();
        }
      }
    });
  }



  that.init = init;
  that.openNameSelectModal = openNameSelectModal;
  that.openDeathAlert = openDeathAlert;
  return that;
};
/* global PylonClient, EventPublisher, $ */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.ResourceLoader
 * @description Loads static data from the server, this sattic data contains
 *    the shape and color of all actors and other parameters necessary for 
 *    drawing.
 */
PylonClient.ResourceLoader = function(){
  var that = new EventPublisher();
  var data;

  function init(){
    data = {};
    return that;
  }

  function onJSON(jsonData){
    data.jsonData = jsonData;
    that.notifyAll('resourcesReady',data);
  }

  function loadJSON(){
    var url = '/js/dist/gameData.json';
    $.getJSON(url,onJSON);
  }
  function load(){
    loadJSON();
  }


  that.init = init;
  that.load = load;
  return that;
};
/* global PylonClient, EventPublisher, io */

/**
 * @namespace PylonClient
 * @memberof! PylonClient
 * @function PylonClient.WebSocket
 * @description Module that interacts with socket.io. Receives data from the 
 *    server and passes it on to the Deserializer. 
 */
PylonClient.WebSocket = function(){
  var that = new EventPublisher();
  var socket;
  var initdata;

  function sendInput(action,state){
    socket.emit('input',{
      a:action,
      s:state
    });
  }

  function login(data){
    socket.emit('login',data);
  }

  function init(){
    socket = io();
    socket.on('connect',function(){
      console.log('socket connect');
    });
    socket.on('init',function(data){
      initdata = data;
      that.notifyAll('ready');
    });
    socket.on('death',function(data){
      that.notifyAll('death',data);
    });
    socket.on('disconnect',function(){
      that.notifyAll('disconnect');
      console.log('socket disconnect');
    });
    socket.on('error',function(error){
      console.log(error);
    });
    socket.on('update',function(data){
      that.notifyAll('update',data);
    });
    socket.on('debug',function(message){
      console.log(message);
    });

    return that;
  }
  function getId(){
    if(!initdata){
      return null;
    }
    return initdata.id;
  }


  that.init = init;
  that.sendInput = sendInput;
  that.login = login;
  that.getId = getId;
  
  return that;
};