/* global PylonClient */

/**
 * Wrapper that allows use in node and in the Browser
 */
(function(exports){
  exports.StaticData = function(options){
    var CG = {
      SHIP:2,
      PROJECTILE:4,
      WORLD:8,
      BACKGROUND:16
    };
    var BODYTYPE = {
      DYNAMIC:1,
      STATIC:2,
      KINEMATIC:4
    };

    var data;
    var that = {};

    function init(){
      data = options.data;
      return that;
    }

    function extend(_target, source){
      var prop;
      var target = _target || {};
      for (prop in source) {
        if (Object.prototype.toString.call(source[prop]) === '[object Object]') {
          target[prop] = extend(target[prop], source[prop]);
        } else {
          target[prop] = source[prop];
        }
      }
      return target;
    }



    function clone(obj){
      return JSON.parse(JSON.stringify(obj));
    }
    function getCG(name){
      return CG[name.toUpperCase()] || 1;
    }
    function getBodyType(name){
      return BODYTYPE[name.toUpperCase()] || 1;
    }


    function convertValues(obj){
      var mask;
      if(obj.collisionGroup && typeof obj.collisionGroup != 'number'){
        obj.collisionGroup = getCG(obj.collisionGroup);
      }
      if(obj.collisionMask && typeof obj.collisionMask != 'number'){
        mask = 0;
        obj.collisionMask.forEach(function(name){
          mask |= getCG(name);
        });
        obj.collisionMask = mask;
      }
      if(obj.bodyOptions && typeof obj.bodyOptions.type != 'number'){
        obj.bodyOptions.type = getBodyType(obj.bodyOptions.type);
      }
    }


    function process(obj){
      var base = {};
      if(obj.extends){
        base = load(obj.extends);  
      }
      extend(base,obj);
      convertValues(base);
      return base;
    }
    function load(name){
      var obj;
      if(data[name]){
        obj = data[name];
        obj = clone(obj);
        obj = process(obj);
        return obj;
      }
      console.log('Data for '+name+' not found');
      return null;
    }


    that.init = init;
    that.load = load;
    return that;
  };
}(typeof exports === 'undefined' ? PylonClient : exports));
