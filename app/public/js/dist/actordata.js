/* global PylonClient */

/**
 * Wrapper that allows use in node and in the Browser
 */
(function(exports){
  exports.ActorData = function(){
    var CG = {
      SHIP:2,
      PROJECTILE:4,
      WORLD:8,
      BACKGROUND:16
    };
    var data = {
      'ship_1':{
        path:[[-1, -1],[0, 1],[1, -1]],
        fillColor:'#FF0000',
        strokeColor:'#000',
        collisionGroup:CG.SHIP,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP
      },
      'ship_2':{
        path:[[-2,0],[0,-1],[2,0],[1,2],[1,1],[0.5,0.5],[-0.5,0.5],[-1,1],[-1,2]],
        fillColor:'#a0c7ce',
        strokeColor:'#2d5052',
        collisionGroup:CG.SHIP,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP,
        weapons:[
          {
            projectile:'projectile_2',
            offset:[1,2.3],
            cooldown:0.6,
            current_cooldown:0.6,
            energy:10
          },
          {
            projectile:'projectile_2',
            offset:[-1,2.3],
            cooldown:0.6,
            current_cooldown:0.6,
            energy:10
          }
        ],
        MAX_HEALTH:200,
        MAX_ENERGY:200,
        acceleration:50.0,
        turningForce:30.0
      },
      'ship_3':{
        path:[
          [3.0,0.0],[5.5,0.0],[6.5,0.5],[7.5,1.5],[8.0,2.5],
          [8.5,5.0],[7.0,2.5],[6.0,2.0],[5.0,3.0],[5.0,4.0],
          [4.25,6.0],[3.5,4.0],[3.5,3.0],[2.5,2.0],[1.5,2.5],
          [0.0,5.0],[0.5,2.5],[1.0,1.5],[2.0,0.5]
        ],
        fillColor:'#f0a7ae',
        strokeColor:'#cd5052',
        collisionGroup:CG.SHIP,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP,
        weapons:[
          {
            projectile:'projectile_2',
            offset:[0.0,5.0],
            cooldown:0.6,
            current_cooldown:0.6,
            energy:10,
            angle:-0.1
          },
          {
            projectile:'projectile_3',
            offset:[2.5,2.0],
            cooldown:0.6,
            current_cooldown:0.6,
            energy:20
          },
          {
            projectile:'projectile_3',
            offset:[6.0,2.0],
            cooldown:1.0,
            current_cooldown:1.0,
            energy:20
          },
          {
            projectile:'projectile_2',
            offset:[8.5,5.0],
            cooldown:0.6,
            current_cooldown:0.6,
            energy:10,
            angle:0.1
          }
        ],
        MAX_HEALTH:400,
        MAX_ENERGY:400,
        acceleration:50.0,
        turningForce:30.0
      },
      'block_1':{
        path:[[0,0],[0, 1],[1, 1],[1,0]],
        fillColor:'#00F',
        strokeColor:'#000',
        collisionGroup:CG.WORLD,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP
      },
      'wall_1':{
        path:[[0,0],[0, 20],[1, 20],[1,0]],
        fillColor:'#CCF',
        strokeColor:'#000',
        collisionGroup:CG.WORLD,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP
      },
      'wall_2':{
        path:[[0,0],[0, 200],[0.5, 200],[0.5,0]],
        fillColor:'#CCF',
        strokeColor:'#000',
        collisionGroup:CG.WORLD,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP
      },
      'wall_3':{
        path:[[0,0],[200, 0],[200, 0.5],[0,0.5]],
        fillColor:'#CCF',
        strokeColor:'#000',
        collisionGroup:CG.WORLD,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP
      },
      'projectile_1':{
        path:[[0,0],[0, 0.5],[0.1, 0.5],[0.1,0]],
        fillColor:'#FFF000',
        strokeColor:'#FFF000',
        collisionGroup:CG.PROJECTILE,
        collisionMask: CG.SHIP | CG.WORLD
      },
      'projectile_2':{
        path:[[0,0],[0, 1],[0.05, 1],[0.05,0]],
        fillColor:'#00C6F3',
        strokeColor:'#00C6F3',
        collisionGroup:CG.PROJECTILE,
        collisionMask: CG.SHIP | CG.WORLD,
        damage:20,
        lifetime:3,
        speed:30
      },
      'projectile_3':{
        path:[[0,0],[0.1, 1.3],[0.15, 1.3],[0.15,0]],
        fillColor:'#00C623',
        strokeColor:'#00C623',
        collisionGroup:CG.PROJECTILE,
        collisionMask: CG.SHIP | CG.WORLD,
        damage:80,
        lifetime:4,
        speed:20
      },
      'background_1':{
        path:[[0,0],[0, 5],[5, 5],[5,0]],
        fillColor:'#CFC',
        strokeColor:'#000',
        collisionGroup:CG.BACKGROUND,
        collisionMask: 0
      },
      'fragment_1':{
        path:[[2,0],[1,2],[1,1],[0.5,0.5]],
        fillColor:'#a0c7ce',
        strokeColor:'#2d5052',
        collisionGroup:CG.SHIP,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP,
        lifetime:7
      },
      'fragment_2':{
        path:[[-1.25,-0.6],[0,-1],[1.25,-0.6],[0.5,0.5],[-0.5,0.5]],
        fillColor:'#a0c7ce',
        strokeColor:'#2d5052',
        collisionGroup:CG.SHIP,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP,
        lifetime:7
      },
      'fragment_3':{
        path:[[-1.25,-0.6],[0,-1],[1.25,-0.6],[0.5,0.5],[-0.5,0.5]],
        fillColor:'#a0c7ce',
        strokeColor:'#2d5052',
        collisionGroup:CG.SHIP,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP,
        lifetime:7
      },
      'resource_1':{
        path:[[-0.2,0],[0,-0.3],[0.2,0],[0,0.3]],
        fillColor:'#c0f7fe',
        strokeColor:'#999',
        collisionGroup:CG.WORLD,
        collisionMask: CG.SHIP
      },
      'default':{
        path:[[0,0],[0, 1],[1, 1],[1,0]],
        fillColor:'#FF69B4',
        strokeColor:'#000',
        collisionGroup:CG.WORLD,
        collisionMask: CG.PROJECTILE | CG.WORLD | CG.SHIP
      }
    };
    var that = {};
    function clone(obj){
      return JSON.parse(JSON.stringify(obj));
    }
    that.getActorData = function(name){
      if(data[name]){
        return clone(data[name]);
      }
      console.error('Actor '+name+' not found');
      return data['default'];
    };
    that.init = function(){
      return that;
    };
    return that;
  };
}(typeof exports === 'undefined' ? PylonClient : exports));
