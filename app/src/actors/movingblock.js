var Actor = require('./actor');

/**
 * @namespace Actors
 * @memberof! Actors
 * @function MovingBlock
 * @description Defines a simple, moving block in the game world that 
 *    collides with other actors.  
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  var lifetime;

  
 
  function init(){
    that.actor = new Actor({
      className:options.className || 'fragment_default',
      position:options.position,
      angle:options.angle
    }).init();
    that.actor.setParent(that,that.actor);

    that.getBody().velocity = options.velocity || [0,0];
    lifetime= that.getBlueprint().lifetime;  
    return that;
  }
  function update(dt){
    if(lifetime != null){
      lifetime -= dt;
      if(lifetime < 0){
        that.remove();
      }
    }
    that.actor.update(dt);
  }

  that.init = init;
  that.update = update;

  return that;
};