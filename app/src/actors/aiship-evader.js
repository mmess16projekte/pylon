var AiShip = require('./aiship');
var p2 = require('p2');
var RoundSensor = require('./roundsensor');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function AiShipEvader
 * @description Passive AI Ship, if it sees a hostile ship it will evade it. 
 */
var AiShipEvader = function(options){
  var that = {
    actor:null
  };
  var spawnPosition;
  var currentTarget;
 

  function onArrive(){
    that.setBehavior('wander');
  }

  function onDeath(){
    var newShip = new AiShipEvader({
      position:spawnPosition,
      className:options.className,
      name:options.name
    }).init();
    that.getEventEmitter().emit('spawn',{
      actors:[newShip]
    });
  }

  function onTargetDeath(){
    that.setBehavior('wander');
  }

  function onSensorUpdate(event){
    var actorIndex = event.actors;
    var position;
    var target;
    var tFaction = that.getBlueprint().faction;
    var distance = Infinity;
    var keys = Object.keys(actorIndex);
    var targets = [];
    keys.forEach(function(key){
      var actor = actorIndex[key];
      var oFaction = actor.getBlueprint().faction;
      var type = actor.getBlueprint().type;
      if(oFaction != tFaction && (type == 'ship' || type == 'turret')){
        targets.push(actor);
      }
    });
    if(targets.length == 0){
      that.setBehavior('wander');
      if(currentTarget){
        currentTarget == null;
      }
      return;
    }
    position = that.getBody().position;
    targets.forEach(function(actor){
      var aPos = actor.getBody().position;
      var deltaX = [0,0];
      var aDist;
      p2.vec2.sub(deltaX,aPos,position);
      aDist = p2.vec2.length(deltaX);
      if(aDist<distance){
        target = actor;
        distance = aDist;
      }
    });
    evade(target);
  }
 
  function init(){
    spawnPosition = [];
    p2.vec2.copy(spawnPosition,options.position);
    currentTarget = null;
    that.actor = new AiShip({
      className:options.className,
      position:options.position,
      angle:options.angle,
      name:options.name
    }).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().on('arriveDone',onArrive);
    that.getEventEmitter().on('death',onDeath);
    that.setBehavior('wander');
    initSensor();
    return that;
  }


  function initSensor(){
    var bodyA, bodyB, sensor, constraint;
    bodyA = that.getBody();

    sensor = new RoundSensor({
      className:'sensor_1',
      position:bodyA.position,
      radius:40,
      owner:that
    }).init();
    sensor.getEventEmitter().on('sensorUpdate',onSensorUpdate);

    bodyB = sensor.getBody();
    constraint = new p2.RevoluteConstraint(bodyA,bodyB,{
      worldPivot:bodyA.position
    });
    that.addChild(sensor,constraint);
  }

  function evade(actor){
    if(currentTarget!= null && actor.getBlueprint().id == currentTarget.getBlueprint().id){
      return;
    }

    that.setBehavior('evade',actor);
  }

  function collision(otherActor,collisionSpeed){
    var oType = otherActor.getBlueprint().type;
    if(oType == 'wall'){
      that.setBehavior('arrive',spawnPosition);
    }
    that.actor.collision(otherActor,collisionSpeed);
  }
  function update(dt){
    if(currentTarget && currentTarget.removed){
      onTargetDeath();
    }
    that.actor.update(dt);
  }



  that.update = update;
  that.init = init;
  that.evade = evade;
  that.collision = collision;

  return that;
};
module.exports = AiShipEvader;
