var AiShip = require('./aiship');
var p2 = require('p2');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function AiShipAssasin
 * @description AI Ship that need to be given a target. It will pursue the 
 *    target until either the target dies or this ship dies. 
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  
  var position;
  var target;

  function onArrive(){
    that.remove();
  }

  function onTargetDeath(){
    that.setBehavior('arrive',position);
  }
 
  function init(){
    var tmppos = [];
    position = [];
    p2.vec2.copy(position,options.position);
    p2.vec2.copy(tmppos,options.position);
    that.actor = new AiShip({
      className:'aiship_assasin',
      position:tmppos,
      angle:options.angle,
      name:'assasin bot'
    }).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().on('arriveDone',onArrive);
    return that;
  }

  function kill(actor){
    target = actor;
    that.setBehavior('pursue',target);

  }

  function update(dt){
    if(target && target.removed){
      onTargetDeath();
    }
    that.actor.update(dt);
  }

  that.update = update;
  that.init = init;
  that.kill = kill;


  return that;
};