var Sensor = require('./sensor');
var AiShipAssasin = require('./aiship-assasin');
var TurretAssasin = require('./turret-assasin');
var p2 = require('p2');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function SoftWall
 * @description Background Sensor that spawns assassin bots if a player
 *    comes in contact with it. Creates a "soft" barrier for the game world. 
 */
module.exports = function(options){
  var that = {
    actor:null
  };

  function onSensorUpdate(event){
    var newactor = event.newactor;
    var aFaction, tFaction;
    if(!newactor){
      return;
    }
    aFaction = newactor.getBlueprint().faction;
    tFaction = that.getBlueprint().faction;
    if(aFaction == tFaction){
      return;
    }
    if(newactor.setBehavior){ // is AI Ship
      return;
    }

    reduceSpeed(newactor);
    createAssasins(newactor);
    
  }
  function createAssasins(target){
    var body = that.getBody();
    var targetBody = target.getBody();
    var angle = body.angle;
    var center = targetBody.position;
    var vectors = [[20,-70],[40,-50],[60,-20],[60,20],[40,50],[20,70]];
    if(options.walltype == 'ship'){
      vectors = [[-10,-70],[-10,70],[10,70],[10,-70]];
    }
    vectors.forEach(function(offset){
      var pos = [];
      p2.vec2.rotate(pos,offset,angle);
      p2.vec2.add(pos,pos,center);
      createAssasin(pos,target);
    });
  }

  function reduceSpeed(actor){
    var maxV = 3;
    var body = actor.getBody();
    var absV = p2.vec2.length(body.velocity);
    if(absV > maxV){
      p2.vec2.scale(body.velocity,body.velocity,maxV/absV);
    }
  }


  function createAssasin(position,target){
    var assasin;
    if(options.walltype == 'ship'){
      assasin = new AiShipAssasin({
        position:position
      }).init();
    }
    else {
      assasin = new TurretAssasin({
        position:position
      }).init();
    }
    assasin.kill(target);
    that.getEventEmitter().emit('spawn',{
      actors:[assasin]
    });
    return assasin;
  }

  
 
  function init(){
    that.actor = new Sensor(options).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().on('sensorUpdate',onSensorUpdate);

    return that;
  }
  that.init = init;

  return that;
};