var Sensor = require('./sensor');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Block
 * @description Defines a zone that heals friendly ships if they are in in, 
 *   and damages hostile ships.
 */
module.exports = function(options){
  var that = {
    actor:null
  };

  var actorsInRange;

  function onSensorUpdate(event){
    actorsInRange = event.actors;    
  }
  

  
 
  function init(){
    that.actor = new Sensor(options).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().on('sensorUpdate',onSensorUpdate);
    actorsInRange = {};
    return that;
  }
  function update(dt){
    var aFaction, tFaction, tType;
    aFaction = that.getBlueprint().faction;
    Object.keys(actorsInRange).forEach(function(id){
      var actor = actorsInRange[id];
      tFaction = actor.getBlueprint().faction;
      tType = actor.getBlueprint().type;
      if(tType != 'ship'){
        return;
      }
      if(aFaction == tFaction){
        actor.modules().health().changeBy(20*dt);
      } else {
        actor.modules().health().changeBy(-10*dt);
      }
    });


    that.actor.update(dt);
  }


  that.init = init;
  that.update = update;

  return that;
};