var Actor = require('./actor');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Block
 * @description Defines a simple, non moving block in the game world that 
 *    collides with other actors.  
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  
 
  function init(){
    that.actor = new Actor({
      className:options.className || 'world_default',
      position:options.position,
      angle:options.angle
    }).init();
    that.actor.setParent(that,that.actor);
    return that;
  }
  that.init = init;

  return that;
};