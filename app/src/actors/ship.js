var Actor = require('./actor');
var MovingBlock = require('./movingblock');
var p2 = require('p2');
var Modules = require('../modules');
var util = require('../util');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Ship
 * @description Base Class for ships, initializes modules and passes data to 
 *    them. Defines general ship behavior that is influenced by it modules.  
 */
module.exports = function(options){
  var that = {
    actor:null
  };

  

  
  var modules;
  function init(){
    var blueprint;
    that.actor = new Actor({
      className:options.className || 'ship_default',
      position:options.position,
      angle:options.angle,
      name:options.name
    }).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().setMaxListeners(25);


    
    blueprint = that.getBlueprint();

    modules = new Modules({
      parentShip:that,
      slots:blueprint.slots
    }).init();
    modules.setLoadout(blueprint.loadout);
    modules.health().setToMax();
    return that;
  }

  function update(dt){
    modules.update(dt);
    that.actor.update(dt);
  }

  function collisionDamage(collisionSpeed){
    if(collisionSpeed > 20){
      return 5;
    }
    return 0;
  }

  function collision(otherActor,collisionSpeed){
    var damageClasses = ['block','movingblock','ship'];
    var damage;
    var otherClass = otherActor.getBlueprint().type;
    if(damageClasses.indexOf(otherClass)!==-1){
      damage = collisionDamage(collisionSpeed);
      modules.health().changeBy(-damage,'collision with '+ otherClass,otherActor);
    }
    that.actor.collision(otherActor,collisionSpeed);
  }
  function serializeCreate(){
    var obj = that.actor.serializeCreate();
    obj.name = that.getBlueprint().name;
    return obj;
  }
  function getMeta(){
    return modules.getMeta(); 
  }

  function projectileHit(damage,from){
    var name = 'projectile';
    if(from){
      name = from.getBlueprint().name;
    }
    modules.health().changeBy(-damage,'shot by '+name,from);
  }
  function resourcePickup(){
    modules.health().changeBy(10);
  }

  function createFragments(){
    var body = that.getBody();
    return that.getBlueprint().fragments.map(function(className){
      var position = [];
      var velocity = [];
      p2.vec2.add(position,body.position,util.random_vec2(-2,2));
      p2.vec2.add(velocity,body.velocity,util.random_vec2(-2,2));
      return new MovingBlock({
        className:className,
        position:position,
        velocity:velocity
      }).init();
    });
  }

  function die(message,killer){
    var fragments = createFragments();
    if(killer){
      killer.getEventEmitter().emit('killed',that);
    }
    that.getEventEmitter().emit('spawn',{
      actors:fragments
    });
    that.getEventEmitter().emit('death',{
      message:message || ''
    }); 
    that.remove();
  }

  function getModules(){
    return modules;
  }


  that.init = init;
  that.update = update;
  that.collision = collision;
  that.projectileHit = projectileHit;

  that.modules = getModules;

  that.serializeCreate = serializeCreate;
  
  that.resourcePickup = resourcePickup;
  that.getMeta = getMeta;
  that.die = die;
  return that;
};