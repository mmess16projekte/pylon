var Actor = require('./actor');

/**
 * @namespace Actors
 * @memberof! Actors
 * @function Resource
 * @deprecated
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  
 
  function init(){
   
    that.actor = new Actor({
      className:options.className || 'resource_default',
      position:options.position,
      angle:options.angle
    }).init();
    that.actor.setParent(that,that.actor);

    that.getBody().shapes.forEach(function(shape){
      shape.sensor = true;
    });   
    return that;
  }
 
  function collision(otherActor,collisionSpeed){
    if(otherActor.resourcePickup){
      otherActor.resourcePickup(that);
      that.remove();
    }
    that.actor.collision(otherActor,collisionSpeed);
  }
  that.init = init;
  that.collision = collision;

  return that;
};