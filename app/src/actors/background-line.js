var Actor = require('./actor');
var p2 = require('p2');
var util = require('../util');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function BackgroundLine
 * @description Defines a simple, non moving line in the background. 
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  
 
  function init(){
    var from = options.from;
    var to = options.to;

    var pos = util.center([from,to]);
    var angle = util.angle(from,to);

    var length = p2.vec2.distance(from,to);
    var thickness = 0.02;
    var path =[[0,0],[0,length],[thickness,length],[thickness,0]];


    that.actor = new Actor({
      className:options.className,
      path:path,
      position:pos,
      angle:angle
    }).init();
    that.actor.setParent(that,that.actor);
    return that;
  }
  that.init = init;
  return that;
};