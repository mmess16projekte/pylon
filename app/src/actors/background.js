var Actor = require('./actor');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Background
 * @description Defines a simple, non moving object in the background. 
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  
 
  function init(){
    that.actor = new Actor(options).init();
    that.actor.setParent(that,that.actor);
    return that;
  }
  that.init = init;
  return that;
};