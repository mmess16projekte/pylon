var Turret = require('./turret');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function TurretAssassin
 * @description Turret the has to be provided a target,
 *     if the target is is range it will shott at it.
 * @ununsed 
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  
  function done(){
    that.die();
  }
 
  function init(){
    that.actor = new Turret({
      position:options.position,
      angle:options.angle,
      className:'turret_base_1',
      gun:'turret_gun_1',
      name:'turret assasin',
      sensor:100
    }).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().on('targetLost',done);
    return that;
  }
  function kill(actor){
    actor.getEventEmitter().on('death',done);
    that.actor.kill(actor);
  }

  that.init = init;
  that.kill = kill;
  return that;
};