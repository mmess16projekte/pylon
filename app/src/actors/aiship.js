var Ship = require('./ship');
var p2 = require('p2');
var util = require('../util');

/**
 * @namespace Actors
 * @memberof! Actors
 * @function AiShip
 * @description Defines AI steering behavoirs, based on 
 *    http://www.red3d.com/cwr/steer/gdc99/
 */
var AiShip = function(options){
  var that = {
    actor:null
  };



  var behaviorIndex = {
    seek:seek,
    flee:flee,
    arrive:arrive,
    wander:wander,
    pursue:pursue,
    evade:evade,
    none:none
  };

  var currentBehavior;
  var currentTarget;

  var maxSpeed;
  var maxForce;
  var fireDistance;
  var evadeDistance;



  function setBehavior(behavior,target){
    switch(behavior){
    case 'arrive':
      currentBehavior = behaviorIndex.arrive;
      currentTarget = target;
      break;
    case 'seek':
      currentBehavior = behaviorIndex.seek;
      currentTarget = target;
      break;
    case 'wander':
      currentBehavior = behaviorIndex.wander;
      currentTarget = target || [0,0];
      break;
    case 'pursue':
      currentTarget = target;
      currentBehavior = behaviorIndex.pursue;
      break;
    case 'evade':
      currentTarget = target;
      currentBehavior = behaviorIndex.evade;
      break;
    case 'none':
      currentBehavior = behaviorIndex.none;
      currentTarget = [0,0];
      break;
    default:
      currentBehavior = behaviorIndex.none;
      console.log('unknown behavior');
    }
   
  }

  function onDeath(){
    
  }
  
  function init(){
    var spawn = util.random_vec2(-50,50);
    var name = options.name || 'bot';
    var blueprint;
    that.actor = new Ship({
      className: options.className,
      position:options.position || spawn,
      angle:options.angle,
      name:name,
      controller:null
    }).init();
    that.actor.setParent(that,that.actor);
    blueprint = that.getBlueprint();
    maxSpeed = blueprint.maxSpeed;
    maxForce = blueprint.maxForce;
    fireDistance = blueprint.fireDistance;
    evadeDistance = blueprint.evadeDistance;

    that.getEventEmitter().on('death',onDeath);
    setBehavior('none');
    return that;
  }

  function none(body){
    var desiredV = [0,0];
    p2.vec2.negate(desiredV,body.velocity);
    return desiredV;
  }

  function seek(body,target){
    var desiredV = [0,0];
    var deltaV = [0,0];

    p2.vec2.sub(desiredV,target,body.position);
    p2.vec2.normalize(desiredV,desiredV);
    p2.vec2.scale(desiredV,desiredV,maxSpeed);
    p2.vec2.sub(deltaV,desiredV,body.velocity);
    return deltaV;
  }
  function flee(body,target){
    var desiredV = [0,0];
    var deltaV = [0,0];
   

    p2.vec2.sub(desiredV,body.position,target);
    p2.vec2.normalize(desiredV,desiredV);
    p2.vec2.scale(desiredV,desiredV,maxSpeed);
    p2.vec2.sub(deltaV,desiredV,body.velocity);
    return deltaV;
  }

  function predict(body,target){
    var tBody = target.getBody();
    var deltaX = [0,0];
    var distance;
    var targetPoint=[0,0];
    p2.vec2.sub(deltaX,tBody.position,body.position);
    distance = p2.vec2.length(deltaX);
    p2.vec2.scale(targetPoint,tBody.velocity,distance/(maxSpeed*1.5));
    p2.vec2.add(targetPoint,targetPoint,tBody.position);
    return {
      position:targetPoint,
      distance:distance
    };
  }


  function pursue(body,target){
    var pTarget = predict(body,target);
    if(pTarget.distance < fireDistance){
      that.modules().weapons().fire();
    }
    if(pTarget.distance < 5){
      return wander(body,pTarget.position);
    }
    return seek(body,pTarget.position);
  }
  function evade(body,target){
    var pTarget = predict(body,target);
    if(pTarget.distance > evadeDistance){
      return [0,0];
    }
    return flee(body,pTarget.position);
  }


  function arrive(body,target){
    var deltaX = [0,0];
    var distance;
    var speed;
    var desiredV = [0,0];
    p2.vec2.sub(deltaX,target,body.position);
    distance = p2.vec2.length(deltaX);
    if(distance < 5){
      that.getEventEmitter().emit('arriveDone');
    }
    speed = Math.min(distance/0.8,20);
    p2.vec2.scale(desiredV,deltaX,speed/distance);
    p2.vec2.sub(desiredV,desiredV,body.velocity);
    return desiredV;
  }

  function wander(body,wanderTarget){
    var wanderRadius = 2;
    var wanderDistance = 10;
    var wanderJitter = 1;
    var desiredV = [];

    p2.vec2.add(wanderTarget,wanderTarget,util.random_vec2(-wanderJitter,wanderJitter));
    p2.vec2.normalize(wanderTarget,wanderTarget);
    p2.vec2.scale(wanderTarget,wanderTarget,wanderRadius);
    p2.vec2.add(desiredV,wanderTarget,[0,wanderDistance]);
    p2.vec2.rotate(desiredV,desiredV,body.angle);
    p2.vec2.sub(desiredV,desiredV,body.velocity);
    return desiredV;
  }



  function applyForce(body,delta_v,dt){
    var force = [0,0];
    var absV = p2.vec2.length(delta_v);
    var absF = (absV / dt)*body.mass;
    if(absV < 0.01){
      return;
    }
    if(absF > maxForce){
      absF = maxForce;
    }
    p2.vec2.scale(force,delta_v,absF/absV);
    body.force = force;
  }

  function changeAngle(body){
    var v = body.velocity;
    var targetA = Math.atan2(v[1],v[0]) - Math.PI/2;
    var deltaA = normalizeAngle(targetA - body.angle);
    if(Math.abs(deltaA)>Math.PI/32){
      body.angularVelocity = Math.sign(deltaA)*3;
    } else {
      body.angularVelocity = 0;
    }
  }

  
  function update(dt){
    var body = that.getBody();
    var delta_v = currentBehavior(body,currentTarget);
    applyForce(body,delta_v,dt);
    changeAngle(body);
    that.actor.update(dt);
  }

  function normalizeAngle(a){
    var angle = a % (2*Math.PI);
    if(angle < -Math.PI){
      angle += (2*Math.PI);
    }
    if(angle > Math.PI){
      angle -= (2*Math.PI);
    }
    return angle;
  }


 
  


  that.init = init;
  that.update = update;
  that.setBehavior = setBehavior;

  return that;
};
module.exports = AiShip;

