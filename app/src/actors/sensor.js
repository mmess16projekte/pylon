var Actor = require('./actor');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Sensor
 * @description Background Actor that emits events once it collides with other 
 *    actors.
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  var owner;

  var actorsInRange;
 
  function init(){
    var body;
    actorsInRange = {};
    that.actor = new Actor({
      className:options.className || 'sensor_default', 
      position:options.position,
      angle:options.angle,
      bodyShape:options.bodyShape
    }).init();
    that.actor.setParent(that,that.actor);
    owner = options.owner || null;
    body = that.getBody();
    body.fixedRotation = true;
    return that;
  }

  function collision(otherActor,collisionSpeed){
    var id;
    if(otherActor == owner){
      return;
    }
    id = otherActor.getBlueprint().id;
    if(!actorsInRange[id]){
      actorsInRange[id]=otherActor;
      that.getEventEmitter().emit('sensorUpdate',{
        actors:actorsInRange,
        newactor:otherActor
      });
    }
    that.actor.collision(otherActor,collisionSpeed);
  }
  function collisionEnd(otherActor){
    var id = otherActor.getBlueprint().id;
    if(actorsInRange[id]){
      delete actorsInRange[id];
      that.getEventEmitter().emit('sensorUpdate',{
        actors:actorsInRange
      });
    }  
    that.actor.collisionEnd(otherActor);
  }


  function hasChanged(){
    return false;
  }

 

  that.init = init;
  that.collision = collision;
  that.collisionEnd = collisionEnd;

  that.hasChanged = hasChanged;
  //that.serializeCreate = serializeCreate;
  return that;
};