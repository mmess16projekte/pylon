var Actor = require('./actor');
var RoundSensor = require('./roundsensor');
var TurretGun = require('./turretgun');
var MovingBlock = require('./movingblock');
var p2 = require('p2');
var util = require('../util');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Turret
 * @description Base Class for turrets, has a sensor for detecting targets and 
 * creates a Turret gun that will shoot at targets.   
 */

module.exports = function(options){
  var that = {
    actor:null
  };
  var gun;
  var targetId;

  function onSensorUpdate(event){
    var newactor = event.newactor;
    var aFaction, tFaction, tType;
    if(!newactor){
      if(!event.actors[targetId]){
        targetId =0;
        that.getEventEmitter().emit('targetLost');
        gun.idle();
      }
      return;
    }
    aFaction = newactor.getBlueprint().faction;
    tFaction = that.getBlueprint().faction;
    tType = newactor.getBlueprint().type;
    if(aFaction == tFaction || tType != 'ship'){
      return;
    }
    targetId = newactor.getBlueprint().id;
    that.getEventEmitter().emit('targetAquired');
    gun.kill(newactor);
  }
 
  function init(){
    that.actor = new Actor(options).init();
    that.actor.setParent(that,that.actor);
    initSensor();
    initGun();
    return that;
  }

  function initSensor(){
    var bodyA, bodyB, sensor, constraint;
    bodyA = that.getBody();

    sensor = new RoundSensor({
      className:'sensor_1',
      position:bodyA.position,
      radius:options.sensor || 50, 
      owner:that
    }).init();
    sensor.getEventEmitter().on('sensorUpdate',onSensorUpdate);

    bodyB = sensor.getBody();
    constraint = new p2.RevoluteConstraint(bodyA,bodyB,{
      worldPivot:bodyA.position
    });
    that.addChild(sensor,constraint);
  }

  function initGun(){
    var bodyA, bodyB, constraint;
    bodyA = that.getBody();
    gun = new TurretGun({
      className:options.gun || 'turret_gun_1',
      position:bodyA.position,
      owner:that,
      name:options.name
    }).init();
    that.getBlueprint().faction = gun.getBlueprint().faction;
    gun.getEventEmitter().on('spawn',function(data){
      that.getEventEmitter().emit('spawn',data);
    });
    gun.getEventEmitter().on('death',die);

    bodyB = gun.getBody();
    constraint = new p2.RevoluteConstraint(bodyA,bodyB,{
      worldPivot:bodyA.position
    });
    that.addChild(gun,constraint,true);
  }

  function projectileHit(damage,from){
    gun.projectileHit(damage,from);
  }
  function createFragments(){
    var body = that.getBody();
    return that.getBlueprint().fragments.map(function(className){
      var position = [];
      var velocity = [];
      p2.vec2.add(position,body.position,util.random_vec2(-2,2));
      p2.vec2.add(velocity,body.velocity,util.random_vec2(-2,2));
      return new MovingBlock({
        className:className,
        position:position,
        velocity:velocity
      }).init();
    });
  }

  function die(){
    var fragments = createFragments();
    that.getEventEmitter().emit('spawn',{
      actors:fragments
    });
    that.getEventEmitter().emit('death',{
      message:''
    }); 
    that.remove();
  }
  function kill(target){
    gun.kill(target);
  }


  that.init = init;
  that.projectileHit = projectileHit;
  that.kill = kill;
  that.die = die;

  return that;
};