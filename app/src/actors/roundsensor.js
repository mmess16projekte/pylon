var Sensor = require('./sensor');
var p2 = require('p2');

/**
 * @namespace Actors
 * @memberof! Actors
 * @function RoundSensor
 * @description Invisible child actor that is used by actors to detect other 
 *    actors in range. Emits events once it collides with other actors. 
 */

module.exports = function(options){
  var that = {
    actor:null
  };
  
 
  function init(){
    var radius = options.radius || 1;
    var circleShape = new p2.Circle({ radius: radius});
    options.bodyShape = circleShape;
    that.actor = new Sensor(options).init();
    that.actor.setParent(that,that.actor);

    return that;
  }

  function hasChanged(){
    return false;
  }

  function serializeCreate(){
    return {
      t:5
    };
  }
  that.init = init;
  that.hasChanged = hasChanged;
  that.serializeCreate = serializeCreate;
  return that;
};