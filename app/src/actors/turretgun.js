var Modules = require('../modules');
var Actor = require('./actor');
var p2 = require('p2');



/**
 * @namespace Actors
 * @memberof! Actors
 * @function TurretGun
 * @description A sort of fake ship that is used in Turrets. Can not move, 
 *    only rotate. Defines AI Behaviour for predicting a targets location and 
 *    shooting it. 
 */
module.exports = function(options){
  var that = {
    actor:null
  };

  
  var target;
  var modules;

  function kill(actor){
    target = actor;
  }
  function idle(){
    target = null;
  }
 
  function init(){
    var blueprint;
    that.actor = new Actor(options).init();
    that.actor.setParent(that,that.actor);
    blueprint = that.getBlueprint();
    modules = new Modules({
      parentShip:that,
      slots:blueprint.slots
    }).init();
    modules.setLoadout(blueprint.loadout);
    modules.health().setToMax();
    
    return that;
  }

  function predict(body,target){
    var tBody = target.getBody();
    var deltaX = [0,0];
    var distance;
    var targetPoint=[0,0];
    p2.vec2.sub(deltaX,tBody.position,body.position);
    distance = p2.vec2.length(deltaX);
    p2.vec2.scale(targetPoint,tBody.velocity,distance/30);
    p2.vec2.add(targetPoint,targetPoint,tBody.position);
    return {
      position:targetPoint,
      distance:distance
    };
  }
  function turn(body,targetPoint){
    var deltaX = [0,0];
    var targetA, deltaA;
    p2.vec2.sub(deltaX,targetPoint,body.position);
    targetA = Math.atan2(deltaX[1],deltaX[0]) - Math.PI/2;
    deltaA = normalizeAngle(targetA - body.angle);
    if(Math.abs(deltaA)>Math.PI/32){
      body.angularVelocity = Math.sign(deltaA)*2;
    } else {
      body.angularVelocity = 0;
    }
    return deltaA;
  }

  function normalizeAngle(a){
    var angle = a % (2*Math.PI);
    if(angle < -Math.PI){
      angle += (2*Math.PI);
    }
    if(angle > Math.PI){
      angle -= (2*Math.PI);
    }
    return angle;
  }


  function update(dt){
    var body = that.getBody();
    var pTarget;
    var deltaA;
    if(target){
      pTarget = predict(body,target);
      deltaA = turn(body,pTarget.position);
      if(pTarget.distance < 50 && Math.abs(deltaA) < Math.PI/16){
        modules.weapons().fire();
      }
      
    }
    modules.update(dt);
    that.actor.update(dt);
  }
  function projectileHit(damage){
    modules.health().changeBy(-damage);
  }

  function die(){
    that.getEventEmitter().emit('death'); 
  }


  that.init = init;
  that.kill = kill;
  that.idle = idle;
  that.die = die;
  that.update = update;
  that.projectileHit = projectileHit;

  return that;
};