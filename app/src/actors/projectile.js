var Actor = require('./actor');
var p2 = require('p2');


/**
 * @namespace Actors
 * @memberof! Actors
 * @function Projectile
 * @description Defines a projectile that moves with constant speed in one 
 *    direction until its lifetime is expired or it hits something.
 */
module.exports = function(options){
  var that = {
    actor:null
  };
  var lifetime;
  var damage;
  var owner;

 
  function init(){
    var body;
    var blueprint;
    var velocity;
  
    owner = options.owner || null;
   
    that.actor = new Actor({
      className:options.className || 'projectile_default',
      position:options.position,
      angle:options.angle
    }).init();
    that.actor.setParent(that,that.actor);
  

    blueprint = that.getBlueprint();
    lifetime = blueprint.lifetime;
    damage = blueprint.damage;
   
    
    body = that.getBody();
    body.damping = 0.0;

    velocity = [];
    p2.vec2.rotate(velocity,[0,blueprint.speed],body.angle);
    body.velocity = [0,0];
    if(options.parentVelocity){
      p2.vec2.copy(body.velocity,options.parentVelocity);
    }
    p2.vec2.add(body.velocity,body.velocity,velocity);    
    return that;
  }
  function update(dt){
    lifetime -= dt;
    if(lifetime <= 0){
      that.remove();
    }
    that.actor.update();
  }
  function collision(otherActor,collisionSpeed){
    var tFaction = owner.getBlueprint().faction;
    var oFaction = otherActor.getBlueprint().faction;
    var isPlayer = owner.getBlueprint().isPlayer;
    if(tFaction == oFaction && !isPlayer){
      return;
    }
    if(owner.getBlueprint().id == otherActor.getBlueprint().id){
      return;
    }

    that.remove();
    that.actor.collision(otherActor,collisionSpeed);
    otherActor.projectileHit(damage,owner);
  }
  function hasChanged(){
    return false;
  }
  function serializeCreate(){
    var obj = that.actor.serializeCreate();
    obj.v = that.getBody().velocity;
    obj.ip = true;
    return obj;
  }


  that.init = init;
  that.update = update;
  that.collision = collision;
  that.hasChanged = hasChanged;
  that.serializeCreate = serializeCreate;

  return that;
};