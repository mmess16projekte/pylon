var Ship = require('./ship');

/**
 * @namespace Actors
 * @memberof! Actors
 * @function PlayerShip
 * @description Defines a ship controlled by a player, adds methods for faction 
 *    reputation and passing inputs to the modules. 
 */
var PlayerShip = function(options){
  var that = {
    actor:null
  };

  var controller;

  var inputStatus = {
    right:0,
    left:0,
    up:0,
    down:0,
    fire:0
  };
  var reputation;
 
  var messages;
  var messagesChanged;

  function addMessage(message){
    messages.push({
      m:message,
      lt:3
    });
    messagesChanged = true;
  }
  function updateMessages(dt){
    messages = messages.filter(function(message){
      message.lt -= dt;
      if(message.lt < 0){
        messagesChanged = true;
        return false;
      }
      return true;
    });
  }
  function upgrade(type,module){
    var loadout = that.getBlueprint().loadout;
    var upgradeLoadout = [];
    loadout.forEach(function(piece){
      if(piece.in == type){
        piece.module = module;
        upgradeLoadout.push(piece);
      }
    });
    that.modules().setLoadout(upgradeLoadout);
  }
  function changeShip(className){
    var body = that.getBody();
    var pos = body.position;
    var angle = body.angle;
    var noptions = {
      className:className,
      name:options.name,
      position:pos,
      angle:angle,
      reputation:reputation,
      controller:options.controller
    };
    

    var newShip = new PlayerShip(noptions).init();
    that.getEventEmitter().emit('spawn',{
      actors:[newShip]
    });
    options.controller.setShip(newShip);
    that.remove();
  }


  function levelUp(faction,level){
    var tFaction = that.getBlueprint().faction;
    addMessage('You reached level '+level+' with faction '+faction);
    if(faction != tFaction){
      return;
    }
    if(faction == 'red'){
      switch(level){
      case 1:
        break;
      case 2:
        addMessage('Your Weapons have been upgraded');
        upgrade('weapons','weapon_2');
        break;
      case 3:
        addMessage('Your Ship has been upgraded');
        changeShip('ship_2_red');
        break;
      case 4:
        addMessage('Your Weapons have been upgraded');
        upgrade('weapons','weapon_2x');
        break;
      case 5:
        addMessage('Your Ship has been upgraded');
        changeShip('ship_3_red');
        break;
      default:
        break;
      }
    }
    if(faction == 'blue'){
      switch(level){
      case 1:
        break;
      case 2:
        addMessage('Your Health has been upgraded');
        upgrade('health','health_3');
        break;
      case 3:
        addMessage('Your Ship has been upgraded');
        changeShip('ship_2_blue');
        break;
      case 4:
        addMessage('Your Health has been upgraded');
        upgrade('health','health_4');
        break;
      case 5:
        addMessage('Your Ship has been upgraded');
        changeShip('ship_3_blue');
        break;
      default:
        break;
      }
    }
    if(faction == 'yellow'){
      switch(level){
      case 1:
        break;
      case 2:
        addMessage('Your Energy has been upgraded');
        upgrade('energy','energy_2');
        break;
      case 3:
        addMessage('Your Ship has been upgraded');
        changeShip('ship_2_yellow');
        break;
      case 4:
        addMessage('Your Energy has been upgraded');
        upgrade('energy','energy_3');
        break;
      case 5:
        addMessage('Your Ship has been upgraded');
        changeShip('ship_3_yellow');
        break;
      default:
        break;
      }
    }
  }

  

  function onInput(input){
    var action = input.a;
    var status = input.s;
    inputStatus[action] = status;
  }
  function onKill(victim){
    var faction = victim.getBlueprint().faction;
    var name = victim.getBlueprint().name;
    addMessage('killed '+name);
    switch(faction){
    case 'red':
      changeReputation('red',-20);
      changeReputation('blue',8);
      changeReputation('yellow',5);
      break;
    case 'blue':
      changeReputation('red',5);
      changeReputation('blue',-20);
      changeReputation('yellow',8);
      break;
    case 'yellow':
      changeReputation('red',8);
      changeReputation('blue',5);
      changeReputation('yellow',-20);
      break;
    default:
      changeReputation('red',3);
      changeReputation('blue',3);
      changeReputation('yellow',3);
      break;
    }
    updateFaction();
  }
  
  function init(){
    that.actor = new Ship(options).init();
    that.actor.setParent(that,that.actor);
    that.getEventEmitter().on('killed',onKill);
    that.getBlueprint().isPlayer = true;

    messages = [];
    messagesChanged = true;

    if(options.reputation){
      reputation = options.reputation;
    } else {
      reputation = {};
      reputation.red = {v:0,l:0,max:10};
      reputation.blue = {v:0,l:0,max:10};
      reputation.yellow = {v:0,l:0,max:10};
    }
    reputation.hasChanged = true;

    controller = options.controller;
    if(controller){
      controller.on('input',onInput);
    }
    
    return that;
  }

  function remove(){
    if(controller){
      controller.removeListener('input',onInput);
    }
    controller = null;
    that.actor.remove();
  }

  function serializeCreate(){
    var obj = that.actor.serializeCreate();
    obj.cid = (controller)?controller.id:0;
    obj.name = that.getBlueprint().name;
    return obj;
  }
  function update(dt){
    var modules =  that.modules();
    modules.engines().move(inputStatus);
    if(inputStatus.fire !== 0){
      modules.weapons().fire();
    }
    updateMessages(dt);
    modules.update(dt);
    that.actor.update(dt);
  }

  function changeReputation(faction,amount){
    var rep, level, max;
    if(!reputation[faction]){
      return;
    }
    rep = reputation[faction].v + amount;
    level = reputation[faction].l;
    max = reputation[faction].max;
    reputation.hasChanged = true;
    if(rep > max){
      if(level>=5){
        rep = max;
      } else {
        level+=1;
        rep -= max;
        max*=1.5;
        levelUp(faction,level);
      }
    }
    if(rep < 0){
      if(level <=0){
        rep = 0;
      } else {
        level -=1;
        max /= 1.5;
        rep += max;
      }
    }
    reputation[faction].v = rep;
    reputation[faction].l = level;
    reputation[faction].max = max;
  }
  function updateFaction(){
    var level;
    var maxLevel =0;
    var maxRep = 0;
    var maxFaction,rep;
    var keys = ['red','blue','yellow'];
    keys.forEach(function(faction){
      rep = reputation[faction];
      if(rep.l > maxLevel || (rep.l == maxLevel && rep.v > maxRep)){
        maxLevel = rep.l;
        maxRep = rep.v;
        maxFaction = faction; 
      }
    });
    if(maxLevel < 1){
      maxFaction = 'white';
    }
    if(that.getBlueprint().faction != maxFaction){
      that.getBlueprint().faction = maxFaction;
      reputation.maxFaction = maxFaction;
      addMessage('You now belong to faction '+maxFaction);
      for(level = 2;level<=maxLevel;level++){
        levelUp(maxFaction,level);
      }
    }

   
  }


  function getMeta(){
    var meta = that.actor.getMeta();
    if(reputation.hasChanged){
      reputation.hasChanged = false;
      meta = meta || {};
      meta.rep = reputation;
    }
    if(messagesChanged){
      messagesChanged = false;
      meta = meta || {};
      meta.messages = messages.map(function(message){
        return message.m;
      });
    }
    return meta;
  }



 


  that.init = init;
  that.serializeCreate = serializeCreate;
  that.remove = remove;
  that.update = update;
  that.getMeta = getMeta;
  that.changeReputation = changeReputation;
  return that;
};

module.exports = PlayerShip;