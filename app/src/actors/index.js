module.exports = function(){
  return {
    Projectile:require('./projectile'),
    Sensor:require('./sensor'),
    SoftWall:require('./softwall'),
    HealingPad:require('./healingpad'),
    AiShipAssasin:require('./aiship-assasin'),
    AiShipAgressive:require('./aiship-agressive'),
    AiShipEvader:require('./aiship-evader'),
    AiShip:require('./aiship'),
    PlayerShip:require('./playership'),
    TurretGun:require('./turretgun'),
    Ship:require('./ship'),
    Block:require('./block'),
    Background:require('./background'),
    BackgroundLine:require('./background-line'),
    MovingBlock:require('./movingblock'),
    Resource:require('./resource'),
    Turret:require('./turret'),
    RoundSensor:require('./roundsensor')
  };
};