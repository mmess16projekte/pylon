var p2 = require('p2');
var shortid = require('shortid');
var staticData = require('../staticData')();
var EventEmitter = require('events').EventEmitter;

/**
 * @namespace Actors
 * @memberof! Actors
 * @function Actor
 * @description Base class for all actors in a kind of OOP sense.
 *    Each Actor will inherit the methods of this class if it does not provide 
 *    its own. This creates a baseline interface for actors removes the need to
 *    check if certain methods exist. 
 */
module.exports = function(options){
  var that = {};

  var blueprint;
  var eventEmitter;
  var body;

  var children;


  function buildBody(){
    var bodyOptions = blueprint.bodyOptions;
    body = new p2.Body(bodyOptions);
    if(blueprint.bodyShape){
      body.addShape(blueprint.bodyShape);
    } else {
      body.fromPolygon(blueprint.path.slice(0));
    }
    body.shapes.forEach(function(shape){
      shape.sensor = blueprint.sensor;
      shape.collisionGroup = blueprint.collisionGroup;
      shape.collisionMask = blueprint.collisionMask;
    });
    //body.adjustCenterOfMass();
    body.parentActor = that;
    body.com = [0,0];
    p2.vec2.sub(body.com,body.position,bodyOptions.position);
    body.position = bodyOptions.position;
    body.angle = bodyOptions.angle;
  }

  function init(){
    var className = options.className || 'actor_default';
    blueprint = staticData.load(className);
    blueprint.className = className;

    if(options.position){
      blueprint.bodyOptions.position = options.position;
    }
    if(options.angle){
      blueprint.bodyOptions.angle = options.angle;
    }
    if(options.name){
      blueprint.name = options.name;
    }
    if(options.bodyShape){
      blueprint.bodyShape = options.bodyShape;
    }
    if(options.faction){
      blueprint.faction = options.faction;
    }
    if(options.path){
      blueprint.path = options.path;
    }
    blueprint.id = shortid.generate();
    children = [];
    eventEmitter = new EventEmitter();
    buildBody();

    return that;
  }

  function getEventEmitter(){
    return eventEmitter;
  }
  function hasChanged(){
    return body.type != p2.Body.STATIC;
  }
  function getBlueprint(){
    return blueprint;
  }
  function getBody(){
    return body;
  }

  function addChild(actor,constraint,visable){
    children.push({
      actor:actor,
      constraint:constraint,
      visable:visable
    });
  }
  function getChildren(){
    return children;
  }



  function serialize(){
    var obj ={};
    obj.t = 2;
    obj.id = blueprint.id;
    obj.p = body.position;
    obj.a = body.angle;
    return obj;
  }
  function serializeCreate(){
    var obj = serialize();
    obj.t = 1;
    obj.an = blueprint.className;
    obj.com = body.com;
    if(options.path){
      obj.path = options.path;
    }
    return obj;
  }
  function serializeRemove(){
    var obj = {};
    obj.t=3;
    obj.id = blueprint.id;
    return obj;
  }
  function remove(){
    eventEmitter.emit('remove',{
      actors:[blueprint.id]
    });
    eventEmitter.removeAllListeners();
  }

  function addDefaultMethods(parent,child){
    var keys = Object.keys(child);
    var functions = keys.filter(function(key){
      return typeof child[key] === 'function';
    });
    functions.forEach(function(key){
      if(!parent[key]){
        parent[key] = child[key];
      }
    });
  }
  /**
   * @namespace Actor
   * @memberof! Actor
   * @function setParent
   * @description This should be called by each actor inheriting from this 
   *    module. It checks the methods of the parent module and if this module 
   *    has a method that the other module lacks it copies the method reference 
   *    over. This can be seen an inheriting methods.  
   */
  function setParent(parent,ochild){
    var child = ochild || that;
    addDefaultMethods(parent,child);
    child.getBody().parentActor = parent;
  }
  
  function noop(){}


  that.getEventEmitter = getEventEmitter;
  that.hasChanged = hasChanged;
  that.getBlueprint = getBlueprint;
  that.getBody = getBody;

  that.addChild = addChild;
  that.getChildren = getChildren;




  that.init = init;
  that.update = noop;
  that.collision = noop;
  that.collisionEnd = noop;
  that.projectileHit = noop;
 
  that.serialize = serialize;
  that.serializeCreate = serializeCreate;
  that.serializeRemove = serializeRemove;
  
  that.remove = remove;
  that.setParent = setParent;
  
  return that;
};





