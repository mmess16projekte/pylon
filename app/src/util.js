var p2 = require('p2');

/**
 * @namespace App
 * @function Util
 * @description Contains utility functions for simple vector math. 
 */

function uniform_random(min,max) {
  return Math.random() * (max - min) + min;
}

function random_vec2(min_x,max_x,o_min_y,o_max_y){
  var min_y = o_min_y || min_x;
  var max_y = o_max_y || max_x;
  var x = uniform_random(min_x,max_x);
  var y = uniform_random(min_y,max_y);
  return [x,y];
}

function regular_polygon(corners,radius){
  var deltaAngle = 2*Math.PI/corners;
  var angle;
  var i;
  var path = [];
  for(i=0;i<corners;i++){
    angle = deltaAngle*i+deltaAngle/2;
    path.push([Math.sin(angle)*radius,Math.cos(angle)*radius]);
  }
  path = path.map(roundPoint);
  return path;
}

function add_at_angle(origin,radius,angle){
  var p = [Math.sin(angle)*radius,Math.cos(angle)*radius];
  p[0]+=origin[0];
  p[1]+=origin[1];
  return roundPoint(p);
}


function roundPoint(p){
  return [round(p[0],4),round(p[1],4)];
}
function center(path){
  var c = [0,0];
  path.forEach(function(point){
    p2.vec2.add(c,c,point);
  });
  p2.vec2.scale(c,c,1/path.length);
  return roundPoint(c);
}
function angle(from,to){
  var delta = [];
  p2.vec2.sub(delta,to,from);
  return Math.atan2(delta[1],delta[0])- Math.PI/2;
}


function round(num,prec){
  var fac = Math.pow(10,prec);
  return Math.round(num*fac)/fac;
}



module.exports = {
  uniform_random:uniform_random,
  random_vec2:random_vec2,
  regular_polygon:regular_polygon,
  add_at_angle:add_at_angle,
  center:center,
  angle:angle
};