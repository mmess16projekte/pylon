
var Game = require('./game');
var Level = require('./level');
var Player = require('./player');
var Serializer = require('./serializer');

/**
 * @namespace App
 * @function Main
 * @description Entry point for the app, initializes socket.io and all other 
 *    modules. 
 */

module.exports = function(options){
  var that = {};
  var socketio;
  var serializer;



  var game;
  var level;

  function onWorldUpdate(data){
    serializer.serialize(data);
    serializer.sendData();
  }
  function onLevelReady(level){
    level.actors.forEach(function(actor){
      game.addActor(actor);
    });
  }
  function onPlayerConnected(player){
    serializer.addPlayer(player);
  }
  function onPlayerReady(player){
    game.addActor(player.getShip());
    //serializer.addPlayer(player);
  }
  function onPlayerDisconnect(player){
    serializer.removePlayer(player.id);
  }


  function onConnection(socket){
    var player = new Player({
      socket:socket
    });
    player.on('playerConnected',onPlayerConnected);
    player.on('playerReady',onPlayerReady);
    player.on('disconnect',onPlayerDisconnect);
    player.init();
  }
  function stop(){
    game.stop();
  }
  function start(){
    game.run();
  }

  function init(){
    serializer = new Serializer({

    }).init();


    socketio = options.socketio;
    socketio.on('connection',onConnection);
    game = new Game({

    }).init();
    game.on('worldUpdate',onWorldUpdate);
    level = new Level().init();
    level.on('levelReady',onLevelReady);    
    level.loadLevel();

    return that;
  }
  that.init = init;
  that.stop = stop;
  that.start = start;


  return that;
};