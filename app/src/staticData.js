
var StaticData = require('../public/js/dist/staticData.js').StaticData;
var rawGameData = require('../public/js/dist/gameData');


var staticData = null;

/**
 * @namespace App
 * @function StaticData
 * @description Load in a shared json file between the client and the server 
 *    that contains all static (non changing) data. e.g the ship polygons. 
 *    Makes sure the json file is only loaded once.
 */
module.exports = function(){
  if(!staticData){
    staticData = new StaticData({
      data:rawGameData
    }).init();
  }
  return staticData;
};