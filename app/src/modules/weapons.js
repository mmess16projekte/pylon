var p2 = require('p2');
var Projectile = require('../actors/projectile');

/**
 * @namespace Modules
 * @memberof! Modules
 * @function Weapons
 * @description Handles the Weapon modules and determines which projectiles 
 *    should be fired.  
 */
module.exports = function(options){
  var that = {};

  var energyModule;
  var parentShip;
  var slots;
  



  function init(){
    parentShip = options.parentShip;
    energyModule = options.energy;
    slots = options.slots;
    return that;
  }

  function fireWeapon(slot){
    var bullet,body,spawnPosition,angle,weapon;
    if(!slot.content){
      return null;
    }
    weapon = slot.content;
    if(weapon.timer > 0 || weapon.energy > energyModule.getEnergy()){
      return null;
    }
    energyModule.changeBy(-weapon.energy);
    weapon.timer = weapon.cooldown;
    body = parentShip.getBody();
    spawnPosition = [0,0];
    p2.vec2.sub(spawnPosition,slot.offset,body.com);
    p2.vec2.rotate(spawnPosition,spawnPosition,body.angle);
    p2.vec2.add(spawnPosition,body.position,spawnPosition);
    angle = slot.angle || 0;
    bullet = new Projectile({
      owner:body.parentActor,
      position:spawnPosition,
      parentVelocity:body.velocity,
      angle:body.angle + angle,
      className:weapon.projectile
    }).init();
    return bullet;
  }


  function fire(){
    var bullets = [];
    var bullet;
    slots.forEach(function(slot){
      bullet = fireWeapon(slot);
      if(bullet){
        bullets.push(bullet);
      }
    });
    parentShip.getEventEmitter().emit('spawn',{
      actors:bullets
    });
  }
  function slotUpdate(){
    
  }


  function update(dt){
    slots.forEach(function(slot){
      if(slot.content){
        slot.content.timer -= dt;
      }
    });
  }


  that.init = init;
  that.fire = fire;
  that.update =update;
  that.slotUpdate = slotUpdate;
  return that;
};

