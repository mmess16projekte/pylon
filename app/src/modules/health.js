/**
 * @namespace Modules
 * @memberof! Modules
 * @function Health
 * @description Determines ship health and rate of healing.
 */
module.exports = function(options){
  var that = {};

  var slots;

  var BASE_MAX_HEALTH = 10;
  var BASE_DELTA_HEALTH = 0;
  
  var parentShip;

  var health;
  var max_health;
  var delta_health;


  var hasChanged;
  var lastDamageSource;


  function init(){
    parentShip = options.parentShip;
    slots = options.slots;
    hasChanged = false;
    slotUpdate();
    lastDamageSource = 'unknown';
    return that;
  }

  function slotUpdate(){
    health = 0;
    max_health = BASE_MAX_HEALTH;
    delta_health = BASE_DELTA_HEALTH;
    slots.forEach(function(slot){
      if(!slot.content){
        return;
      }
      max_health += slot.content.max_health;
      delta_health += slot.content.delta_health;
    });
    hasChanged = true;
    setToMax();
  }

  function changeBy(amount,message,from){
    var newHealth = health + amount;
    if(message){
      lastDamageSource = message;
    }
    if(newHealth < 0){
      newHealth = 0;
    }
    if(newHealth > max_health){
      newHealth = max_health;
    }
    if(newHealth != health){
      health = newHealth;
      if(health == 0){
        parentShip.die(lastDamageSource,from);
      }
      hasChanged = true;
      return true;
    }
    return false;
  }
  function setToMax(){
    changeBy(max_health);
  }


  function update(){
    if(delta_health == 0){
      return false;
    }
    return changeBy(delta_health);
  }
  


  function poll(){
    if(!hasChanged){
      return null;
    }
    hasChanged = false;
    return {
      health:health,
      max_health:max_health
    };
  }



  that.init = init;
  that.poll = poll;
  that.slotUpdate = slotUpdate;
  that.update = update;
  that.changeBy = changeBy;
  that.setToMax = setToMax;
  return that;
};

