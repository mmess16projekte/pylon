var p2 = require('p2');

/**
 * @namespace Modules
 * @memberof! Modules
 * @function Engines
 * @description Determines ship acceleration, and turning force. 
 */

module.exports = function(options){
  var that = {};

  var energyModule;
  var body;
  var slots;
  

  var BASE_ACCELERATION = 1;
  var BASE_TURNING_FORCE = 1;
  var BASE_DELTA_ENERGY = 0;

  var acceleration;
  var turningForce;
  var delta_energy;



  function init(){
    body = options.parentShip.getBody();
    energyModule = options.energy;
    slots = options.slots;
    slotUpdate();
    return that;
  }

  function getForce(factor){
    var force = [];
    p2.vec2.rotate(force,[0,factor],body.angle);
    return force;
  }

  function slotUpdate(){
    acceleration = BASE_ACCELERATION;
    turningForce = BASE_TURNING_FORCE;
    delta_energy = BASE_DELTA_ENERGY;
    slots.forEach(function(slot){
      if(!slot.content){
        return;
      }
      acceleration += slot.content.acceleration;
      turningForce += slot.content.turningForce;
      delta_energy += slot.content.energy;
    });
  }

  function update(){
  }

  function move(inputStatus){
    if(inputStatus.right !== 0){
      body.angularForce += turningForce;
    }
    if(inputStatus.left !== 0){
      body.angularForce -= turningForce;
    }
    if(inputStatus.up !== 0){
      if(delta_energy == 0 || energyModule.getEnergy()>delta_energy){
        body.force= getForce(acceleration);
        energyModule.changeBy(-delta_energy);
      }
    }
    if(inputStatus.down !== 0){
      body.force= getForce(-acceleration/2);
    }
  }


  that.init = init;
  that.update =update;
  that.move = move;
  that.slotUpdate = slotUpdate;
  return that;
};

