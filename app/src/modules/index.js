var staticData = require('../staticData')();
var Weapons = require('./weapons');
var Health = require('./health');
var Energy = require('./energy');
var Engines = require('./engines');

/**
 * @namespace Modules
 * @function Modules
 * @description Handles interchangeable modules on the ship. Each ship has slots 
 *     that can be filled with a certain loadout. 
 */
module.exports = function(options){
  var that = {};
  var parentShip;
  var slots;

  
  var subModules = {
    weapons:null,
    health:null,
    energy:null,
    engines:null
  };




  function init(){
    parentShip = options.parentShip;
    slots = options.slots;
    subModules.energy = new Energy({
      slots:slots.energy
    }).init();
    if(slots.weapons){
      subModules.weapons = new Weapons({
        energy:subModules.energy,
        parentShip:parentShip,
        slots:slots.weapons
      }).init();
    }
    if(slots.health){
      subModules.health = new Health({
        parentShip:parentShip,
        slots:slots.health
      }).init();
    }
    if(slots.engines){
      subModules.engines = new Engines({
        energy:subModules.energy,
        parentShip:options.parentShip,
        slots:slots.engines
      }).init();
    }
    return that;
  }

  function setModule(module){
    var slotList = slots[module.in] || [];
    var slot = slotList[module.index];
    var payload = null;
    var subModule;
    if(!slot){
      console.log('invalid module'+module);
      return;
    }
    if(module.module == null){
      slot.content = null;
    } else{
      payload = staticData.load(module.module);
      if(payload.size <= slot.size){
        slot.content = payload;
      } else {
        console.log('invalid module size '+JSON.stringify(module));
      }
    }
    subModule = subModules[module.in];
    subModule.slotUpdate();
  }

  function setLoadout(loadout){
    loadout.forEach(setModule);
  }



  function weapons(){
    return subModules.weapons;
  }
  function energy(){
    return subModules.energy;
  }
  function health(){
    return subModules.health;
  }
  function engines(){
    return subModules.engines;
  }


  function getMeta(){
    var healthObj = that.health().poll();
    var energyObj = that.energy().poll();
    var obj = null;
    if(healthObj){
      obj = obj || {};
      obj.t = 4;
      obj.health = healthObj.health;
      obj.max_health = healthObj.max_health;
    }
    if(energyObj){
      obj = obj || {};
      obj.t = 4;
      obj.energy = energyObj.energy;
      obj.max_energy = energyObj.max_energy;
    }
    return obj;
  }
  function update(dt){
    if(that.health()){
      that.health().update(dt);
    }
    if(that.energy()){
      that.energy().update(dt);
    }
    if(that.weapons()){
      that.weapons().update(dt);
    }
    if(that.engines()){
      that.engines().update(dt);
    }
  }



  that.init = init;
  that.weapons = weapons;
  that.health = health;
  that.energy = energy;
  that.engines = engines;
  that.update = update;
  that.getMeta = getMeta;
  that.setLoadout = setLoadout;
  return that;
};
