/**
 * @namespace Modules
 * @memberof! Modules
 * @function Energy
 * @description Determines ship energy and rate of energy change. 
 */
module.exports = function(options){
  var that = {};

  var slots;

  var BASE_MAX_ENERGY = 10;
  var BASE_DELTA_ENERGY = 0;
  

  var energy;
  var max_energy;
  var delta_energy;

  var hasChanged;


  function init(){
    slots = options.slots;
    hasChanged = false;
    slotUpdate();
    return that;
  }

  function slotUpdate(){
    energy = 0;
    max_energy = BASE_MAX_ENERGY;
    delta_energy = BASE_DELTA_ENERGY;
    slots.forEach(function(slot){
      if(!slot.content){
        return;
      }
      max_energy += slot.content.max_energy;
      delta_energy += slot.content.delta_energy;
    });
    hasChanged = true;
  }

  function changeBy(amount){
    var newEnergy = energy + amount;
    if(newEnergy < 0){
      newEnergy = 0;
    }
    if(newEnergy > max_energy){
      newEnergy = max_energy;
    }
    if(newEnergy != energy){
      energy = newEnergy;
      hasChanged = true;
      return true;
    }
    return false;

  }
  function update(){
    if(delta_energy == 0){
      return false;
    }
    return changeBy(delta_energy);
  }
  function getEnergy(){
    return energy;
  }
  


  function poll(){
    if(!hasChanged){
      return null;
    }
    hasChanged = false;
    return {
      energy:energy,
      max_energy:max_energy
    };
  }



  that.init = init;
  that.poll = poll;
  that.getEnergy = getEnergy;
  that.slotUpdate = slotUpdate;
  that.update = update;
  that.changeBy = changeBy;
  return that;
};

