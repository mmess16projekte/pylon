var actors = require('./actors')();
var EventEmitter = require('events').EventEmitter;
var util = require('./util');
var p2 = require('p2');

/**
 * @namespace App
 * @function Level
 * @description Creates all non player Actors in the Game world, this includes
 *    - AI Ships
 *    - Walls 
 *    - Background Objects/Text
 *    - Turrets
 */
module.exports = function(){
  var that = new EventEmitter();  

  var level = {
    actors:[]
  };

  that.init = function(){
    

    return that;
  };
  function addLine(from,to){
    var line = new actors.BackgroundLine({
      className:'background_line_1',
      from:from,
      to:to
    }).init();
    level.actors.push(line);
  }
  function lineAlong(path){
    var i;
    for(i=0;i<path.length-1;i++){
      addLine(path[i],path[i+1]);
    }
    addLine(path[path.length-1],path[0]);
  }

  function createSoftWall(path){
    var i;
    for(i=0;i<path.length-1;i++){
      addSoftWall(path[i],path[i+1]);
    }
    addSoftWall(path[path.length-1],path[0]);
  }
  function addSoftWall(from,to){
    var length = p2.vec2.distance(from,to);
    var className = 'softwall_2';
    var center = util.center([from,to]);
    var angle = util.angle(from,to);
    var wall, text;
    if(length < 310){
      className = 'softwall_1';
    }
    wall = new actors.SoftWall({
      className:className,
      position:center,
      angle:angle,
      walltype:'ship'
    }).init();
    level.actors.push(wall);
    text = new actors.Background({
      className:'background_text_1',
      position:center,
      angle:angle-Math.PI/2
    }).init();
    level.actors.push(text);
    text = new actors.Background({
      className:'background_text_1',
      position:util.center([center,to]),
      angle:angle-Math.PI/2
    }).init();
    level.actors.push(text);
    text = new actors.Background({
      className:'background_text_1',
      position:util.center([center,from]),
      angle:angle-Math.PI/2
    }).init();
    level.actors.push(text);

  }
  function addHardWall(){
    var wall = new actors.Block({
      className:'wall_3',
      position:[0,500],
      angle:0
    }).init();
    level.actors.push(wall);
    wall = new actors.Block({
      className:'wall_3',
      position:[0,-500],
      angle:0
    }).init();
    level.actors.push(wall);
    wall = new actors.Block({
      className:'wall_3',
      position:[500,0],
      angle:Math.PI/2
    }).init();
    level.actors.push(wall);
    wall = new actors.Block({
      className:'wall_3',
      position:[-500,0],
      angle:Math.PI/2
    }).init();
    level.actors.push(wall);
  }



  function setupLevel(){
    var sidelength = 300;
    var innerTriangle = util.regular_polygon(3,sidelength/Math.sqrt(3));
    var outer = [];
    var centers = [];
    innerTriangle.forEach(function(point,i){
      var aoffset = Math.PI*(2/3)*i;
      var a1 = 0;
      var a2 = Math.PI*(2/3);
      var p1 = util.add_at_angle(point,sidelength,aoffset+a1);
      var p2 = util.add_at_angle(point,sidelength,aoffset+a2);
      var cp = util.add_at_angle(point,Math.sqrt(2)*sidelength*0.5,aoffset-Math.PI/4);
      centers.push(cp);
      addLine(point,p1);
      addLine(point,p2);
      outer.push(p1);
      outer.push(p2);
    });
    lineAlong(innerTriangle);
    lineAlong(outer);
    createSoftWall(outer);
    addHardWall();
  
    setupFaction(centers[0],'red');
    setupFaction(centers[1],'blue');
    setupFaction(centers[2],'yellow');

  }

  function setupFaction(center,faction){
    var base;
    spawnAiShips(center,faction); 
    spawnTurrets(center,faction);

    base = new actors.HealingPad({
      className:'healingpad_1_'+faction,
      position:center
    }).init();
    level.actors.push(base);

  }



  function spawnAiShips(center,faction){
    var ship;
    var position;
    var deltaAngle = 2*Math.PI/3;
    var i;
    var className = 'aiship_1_'+faction;
    var name = faction+' bot';
    for(i=0;i<3;i++){
      position = util.add_at_angle(center,30,deltaAngle*i);
      ship = new actors.AiShipAgressive({
        className:className,
        position:position,
        name:name
      }).init();
      level.actors.push(ship);
      position = util.add_at_angle(center,30,deltaAngle*i+Math.PI/4);
      ship = new actors.AiShipEvader({
        className:'aiship_2_'+faction,
        position:position,
        name:name
      }).init();
      level.actors.push(ship);
    }
  }
  function spawnTurrets(center,faction){
    var gunName = 'turret_gun_1_'+faction;
    var name = faction+' turret';
    var angle = -util.angle(center,[0,0]);
    var p1 = util.add_at_angle(center,100,angle);
    var p2 = util.add_at_angle(center,100,angle+0.4);
    var p3 = util.add_at_angle(center,100,angle-0.4);
    var turret = new actors.Turret({
      position:p1,
      angle:0,
      className:'turret_base_1',
      gun:gunName,
      name:name,
      sensor:50
    }).init();
    level.actors.push(turret);
    turret = new actors.Turret({
      position:p2,
      angle:0,
      className:'turret_base_1',
      gun:gunName,
      name:name,
      sensor:50
    }).init();
    level.actors.push(turret);
    turret = new actors.Turret({
      position:p3,
      angle:0,
      className:'turret_base_1',
      gun:gunName,
      name:name,
      sensor:50
    }).init();
    level.actors.push(turret);
  }

 
  

  that.loadLevel = function(){
    setupLevel();

    that.emit('levelReady',level);
  };


  return that;
};