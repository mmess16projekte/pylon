
/**
 * @namespace App
 * @function Serializer
 * @description Handles actor serialization, and keeps track of relevant data 
 *    for players. Always sends the least amount of data possible. 
 *    Has several different functions:
 *    - If a new client joins it checks for the player position in the world and 
 *      sends him a full copy of every actors state that is in range of the 
 *      player. 
 *    - Each frame it sends partial updates for actors where the client has 
 *      already received the full state of the actor earlier. 
 *    - If a new actor appears in the clients range it will send a copy of 
 *      its full state once.
 *    - If an actor is no longer in the clients range it will send a delete 
 *      package and after that nothing until it comes within range again.
 */
module.exports = function(){
  var that = {};

  var playerIndex;
  var globalData;

  function init(){
    playerIndex = {};
    return that;
  }

  function addPlayer(player){
    playerIndex[player.id] = {
      obj:player,
      knownActors:{},
      frame:[],
      position:[0,0]
    };
  }
  function removePlayer(playerId){
    delete playerIndex[playerId];
  }

  function handleActorRemove(actor,actorId){
    var pack = actor.serializeRemove();
    var player;
    Object.keys(playerIndex).forEach(function(playerId){
      player = playerIndex[playerId];
      if(player.knownActors[actorId]){
        delete player.knownActors[actorId];
        player.frame.push(pack);
      } 
    });
  }
  function serializeActor(actor,actorId){
    var pack;
    var player;
    var known;
    var inRange;
    if(!actor.hasChanged()){
      Object.keys(playerIndex).forEach(function(playerId){
        player = playerIndex[playerId];
        if(!player.knownActors[actorId]){
          player.frame.push(actor.serializeCreate());
          player.knownActors[actorId] = true;
        }
      });
      return;
    }
    pack = actor.serialize();
    Object.keys(playerIndex).forEach(function(playerId){
      player = playerIndex[playerId];
      known = player.knownActors[actorId];
      inRange = isInRange(player.position,pack.p);
      if(known && inRange){
        player.frame.push(pack);
      }
      if(!known && inRange){
        player.frame.push(actor.serializeCreate());
        player.knownActors[actorId] = true;
      }
      if(known && !inRange){
        player.frame.push(actor.serializeRemove());
        delete player.knownActors[actorId];
      }
    });
  }

  function isInRange(playerPosition,packPosition){
    var max_dx = 120;
    var max_dy = 68;
    if(Math.abs(playerPosition[1]-packPosition[1])>max_dy){
      return false;
    }
    if(Math.abs(playerPosition[0]-packPosition[0])>max_dx){
      return false;
    }
    return true;
  }



  function serialize(data){
    var actor;
    var actorIndex = data.actorIndex;
    globalData = data.globalData;
    Object.keys(playerIndex).forEach(function(playerId){
      var player = playerIndex[playerId];
      var ship = player.obj.getShip();
      if(!ship){
        return;
      }
      player.position = ship.getBody().position;
    });


    Object.keys(actorIndex).forEach(function(actorId){
      actor = actorIndex[actorId];
      if(actor.removed){
        handleActorRemove(actor,actorId);
        delete actorIndex[actorId];
        return;
      }
      serializeActor(actor,actorId);
    });
  }
  function sendData(){
    var player;
    var frame;
    var metaData;
    Object.keys(playerIndex).forEach(function(playerId){
      player = playerIndex[playerId];
      frame = player.frame;
      metaData = player.obj.getMetaData();
      if(metaData){
        frame.push(metaData);
      }
      player.obj.send({
        frame:frame,
        global:globalData
      });
      player.frame = [];
    });
  }

  that.init = init;
  that.addPlayer = addPlayer;
  that.removePlayer = removePlayer;
  that.serialize = serialize;
  that.sendData = sendData;
  return that;
};