var actors = require('./actors')();
var EventEmitter = require('events').EventEmitter;


/**
 * @namespace App
 * @function Player
 * @description Handles client joins and leaves, assigns the a ship and passes 
 *    player input to the ship while passing ship meta data to the client. 
 */
module.exports = function(options){
  var that = new EventEmitter();

  var socket;
  var ship;

  function onDeath(data){
    socket.emit('death',data);
  }  

  function onDisconnect(){
    console.log('Player '+that.id+' disconnected');
    if(ship){
      ship.remove();
    }
    that.emit('disconnect',that);
  }
  function onInput(input){
    that.emit('input',input);
  }

  function sanitize(string){
    var regexp = new RegExp('(?=^.{1,10}$)^[a-zA-Z][a-zA-Z0-9]*[._-]?[a-zA-Z0-9]+$');
    if(regexp.test(string)){
      return string;
    }
    return 'invalid';
  }


  function onLogin(data){
    var newShip;
    var name = sanitize(data.name);
    var spawn = [100*Math.random()-50,100*Math.random()-50];
    console.log('A player logged in id: '+socket.id+', name:'+name);

    newShip = new actors.PlayerShip({
      className:'ship_1',
      name:name,
      position:spawn,
      controller:that
    }).init();
    setShip(newShip);
    that.emit('playerReady',that);
  }

  function setShip(newShip){
    ship = newShip;
    ship.getEventEmitter().on('death',onDeath);
  }



  that.init = function(){
    socket = options.socket;
    socket.on('disconnect',onDisconnect);
    socket.on('input',onInput);
    socket.on('login',onLogin);
    that.id = socket.id;
    socket.emit('init',{
      id:that.id
    });
    console.log('A player connected id: '+socket.id);
    that.emit('playerConnected',that);
    
    return that;
  };
  that.getShip = function(){
    return ship;
  };
  that.getMetaData = function(){
    if(ship){
      return ship.getMeta();
    }
    return null;
  };

  that.send = function(data){
    socket.emit('update',data);
  };

  that.sendFrame = function(data){
    var playerFrame = [];
    var meta = null;
    if(ship){
      meta = ship.getMeta();
    }
    data.frame.forEach(function(pack){
      playerFrame.push(pack);
    });
    if(meta){
      playerFrame.push(meta);
    }
    socket.emit('update',{
      dt:data.dt,
      frame:playerFrame
    });
  };
  that.getSocket = function(){
    return socket;
  };
  that.setShip = setShip;

  return that;
};