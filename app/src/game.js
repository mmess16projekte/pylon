var p2 = require('p2');
var EventEmitter = require('events').EventEmitter;

/**
 * @namespace App
 * @function Game
 * @description Creates and steps the game world, handles spawning of new actors
 *    and removal of actors. The game steps the physics at a rate of 30 fps by 
 *    default but will reduce frame rate if under higher load. Physics is not 
 *    bound by frame rate and will happen at constant speed. 
 */
module.exports = function(){
  var that = new EventEmitter();
  var world;
  var interval;
  var timeStep = 1/30;
  var maxSubSteps = 10;
  var lastTime;

  var joinQueue;
  var removeQueue;
  var actorIndex;


  function init(){
    joinQueue = [];
    removeQueue = [];
    actorIndex = {};

    world = new p2.World({
      gravity:[0.0,0.0]
    });
    world.on('postStep',postStep);
    world.on('beginContact',onBeginContact);
    world.on('endContact',onEndContact);
    return that;
  }
  function update(){
    var time = new Date().getTime();
    var deltaTime = lastTime ? (time - lastTime) / 1000 : 0;
    world.step(timeStep, deltaTime, maxSubSteps);
    lastTime = time;
  }
  function run(){
    console.log('running...');
    if(!interval){
      interval = setInterval(update,timeStep*1000);
    }
  }
  function stop(){
    clearInterval(interval);
    interval = null;
  }
  function onBeginContact(contact){
    var bodyA = contact.bodyA;
    var bodyB = contact.bodyB;
    var actorA = bodyA.parentActor;
    var actorB = bodyB.parentActor;
    var deltaV = [];
    var absV;
    p2.vec2.sub(deltaV,bodyA.velocity,bodyB.velocity);
    absV = p2.vec2.length(deltaV);
    actorA.collision(actorB,absV);
    actorB.collision(actorA,absV);
  }
  function onEndContact(contact){
    var bodyA = contact.bodyA;
    var bodyB = contact.bodyB;
    var actorA = bodyA.parentActor;
    var actorB = bodyB.parentActor;
    actorA.collisionEnd(actorB);
    actorB.collisionEnd(actorA);
  }

  function postStep(event){
    var deltaTime = event.target.lastTimeStep || timeStep;
    if(joinQueue.length){
      handleJoins();
    }
    updateActors(deltaTime);
    if(removeQueue.length){
      handleRemoves();
    }
    that.emit('worldUpdate',{
      actorIndex:actorIndex,
      globalData:{
        dt:deltaTime
      }
    });
  }
  function handleJoins(){
    var event;
    var queue = joinQueue;
    joinQueue = [];
    while(queue.length > 0){
      event = queue.shift();
      if(event.type == 'actor'){
        handleActorJoin(event.payload);
      }
    }
  }
  function handleActorJoin(actor){
    var id = actor.getBlueprint().id;
    actorIndex[id] = actor;
    world.addBody(actor.getBody());
    actor.getChildren().forEach(function(child){
      world.addBody(child.actor.getBody());
      world.addConstraint(child.constraint);
      if(child.visable){
        id = child.actor.getBlueprint().id;
        actorIndex[id] = child.actor;
      }
    });

  }

  function updateActors(deltaTime){
    Object.keys(actorIndex).forEach(function(actorId){
      actorIndex[actorId].update(deltaTime);
    });
  }
  function handleRemoves(){
    var event;
    while(removeQueue.length >0){
      event = removeQueue.shift();
      if(event.type == 'actor'){
        handleActorRemove(event.payload);
      }
    }
  }
  function handleActorRemove(actorId){
    var actor = actorIndex[actorId];
    if(!actor || actor.removed){
      return;
    }
    actor.getChildren().forEach(function(child){
      world.removeConstraint(child.constraint);
      world.removeBody(child.actor.getBody());
      child.actor.removed = true;
    });
    world.removeBody(actor.getBody());
    actor.removed = true;
  }



  function onRemove(data){
    if(data.actors){
      data.actors.forEach(function(actorId){
        removeQueue.push({
          type:'actor',
          payload:actorId
        });
      });
    }
  }
  function onSpawn(data){
    if(data.actors){
      data.actors.forEach(addActor);
    }
  }



  function addActor(actor){
    var eventEmitter = actor.getEventEmitter();
    joinQueue.push({
      type:'actor',
      payload:actor
    });
    eventEmitter.on('remove',onRemove);
    eventEmitter.on('spawn',onSpawn);
  }

  function getActorById(id){
    return actorIndex[id];
  }




  that.init = init;
  that.run = run;
  that.stop = stop;
  that.addActor = addActor;
  that.getActorById = getActorById;

  return that;
};