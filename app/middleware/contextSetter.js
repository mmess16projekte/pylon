module.exports = function(context){
  return function(req,res,next){
    res.locals.context = context;
    next();
  };
};