module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    focus: {
      dev: {
        include:['dev','client','json']
      },
      production:{
        include:['production','client']
      }
    },
    concat:{
      options: {
        seperator: ';'
      },
      dist: {
        src: ['public/js/app/pylon-client.js','public/js/app/**/*.js'],
        dest: 'public/js/dist/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      dist:{
        files: {
          'public/js/dist/<%= pkg.name %>.min.js':['<%= concat.dist.dest %>']
        }
      }
    },
    json_merge:{
      dev:{
        files:{
          'public/js/dist/gameData.json':['gameData/**/*.json']
        }
      }
    },
    express:{
      dev:{
        options:{
          script:'app.js',
          node_env: 'development'
        }
      },
      production:{
        options:{
          script:'app.js',
          node_env: 'production'
        }
      }
    },
    watch: {
      dev:{
        files: ['*.js','src/*.js','src/**/*.js'],
        tasks:['express:dev'],
        options:{
          spawn:false
        }
      },
      production:{
        files: ['*.js','src/*.js','src/**/*.js'],
        tasks:['express:production'],
        options:{
          spawn:false
        }
      },
      client:{
        files: ['public/js/app/**/*.js'],
        tasks:['concat','uglify']
      },
      json:{
        files: ['gameData/**/*.json'],
        tasks:['json_merge:dev']
      }
    }
  });

  
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-json-merge');
  grunt.loadNpmTasks('grunt-focus');

  // Default task(s).
  grunt.registerTask('default', ['']);
  grunt.registerTask('dev',['express:dev','watch:dev']);
  grunt.registerTask('all',['express:dev','concat','uglify','json_merge:dev','focus:dev']);
  grunt.registerTask('production',['express:production','concat','uglify','json_merge:dev','focus:production']);
  grunt.registerTask('json',['json_merge:dev']);
};


















