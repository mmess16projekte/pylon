### Pylon

Multiplayer Web Game, Benutzer steuern unterschiedliche 2d ”Raumschiffe” die sie unterschiedlich ausstatten können und versuchen sich gegenseitig und evtl. AI Gegner abzuschießen. Arcade basierte Kontrolle der Schiffe und echtzeit Multispieler. Simpler abstrakter Artstyle, haupsächlich einfache Formen, convexe Polygone und Kreise.
Benutzer rufen die Spielapp auf, wählen einen Nicknamen und gelangen dann direkt in eine 2D Spielwelt in der sie ihr "Schiff" kontrollieren mit vielen anderen Spielern interagieren. Hebt sich von andren Spielen dieser Art ab durch Simulation von komplexer Physik, interaktiver Arcade basierter Steuerung und nicht spielerkontrollierter Aktoren.
Es gibt 3 verschiedene Factionen mit AI Schiffen die sich gegenseitig bekämpfen. Spieler schließen sich einer Faction an indem sie Shiffe der anderen Factionen abschießen und dadurch "Repuation" erhalten. Wenn Spieler genug Reputation mit einer Faction haben erhalten sie Upgrades und andere Schiffe von der Faction. 

## Setup

Um Pylon zu installieren wird eine aktuelle Version von node.js vorausgesetzt. 
node.js ist [hier](https://www.nodejs.org) verfügbar.

Nachdem node.js installiert ist, muss zunächst des repository gecloned werden:
```sh
git clone git@bitbucket.org:mmess16projekte/pylon.git
```
Anschließend wechseln Sie in den app ordner und installieren die notwendigen pakete:
```sh
cd pylon/app
npm install
```
Nun können sie Pylon starten:
```sh
# For development
grunt all
# For production
grunt production
```
Pylon ist nun standartmäßig auf port 3000 des Servers verfügbar. 
<http://myserver.com:3000>
Da die Spielsimlation sehr Resourcenintensiv ist muss sie bei ersten Aufruf gestartet werden indem Sie 
<http://myserver.com:3000/start>
aufrufen.
Die Spielsimulation kann durch Aufruf von 
<http://myserver.com:3000/stop> wieder beendet werden.

## Gehostete Version

Eine spielbare Version vn Pylon ist unter 
<http://132.199.139.24:3000/start> 
verfügbar.


